function [data] = initialize_formIntegrals(data,timeleft)    
% Pre-evaluates the integrals used in the tau integration-by-parts 
% written by Pierson Guthrey
% -------------------------------------------------
% INPUTS    
% OUTPUTS   saves the results in a file
% Note: other variables may be %loaded in from the problem parameter files

warning('set this value')
if ~strcmp(data.linearcase,'constantcoefficient') 
    data.I_futr = zeros(data.thetaT,data.thetaT) ;
    data.I_past = zeros(data.thetaT,data.thetaT);
    Psi_dtau = zeros(data.thetaT,data.thetaT);

    data.I_futr = zeros(data.thetaT,data.thetaT);                
    data.I_past = zeros(data.thetaT,data.thetaT); 
    Psi_dtau = zeros(data.thetaT,data.thetaT);                            

    temp1 = 0;
    temp2 = 0;
    temp3 = 0;

    for k = 1:data.Pd
        v1quad = data.Dlist(k,1); 
        if data.space_dims >= 2
            v2quad = data.Dlist(k,2);
        else
            v2quad = 1;
        end
        if data.space_dims >= 3
            v3quad = data.Dlist(k,3);
        else
            v3quad = 1;
        end
        weight = data.Dquadwgts(k);
        psikplus = data.vectpsi_futr_Trans(:,:,v1quad,v2quad,v3quad);
        psikmnus = data.vectpsi_past_Trans(:,:,v1quad,v2quad,v3quad);
        psil = data.vectpsi_futr(:,:,v1quad,v2quad,v3quad);
        temp1 = temp1 + psikplus*psil*weight;
        temp2 = temp2 + psikmnus*psil*weight;
    end

    for k = 1:data.Pdp1
        tquad = data.Dp1list(k,1); 
        v1quad = data.Dp1list(k,2); 
        if data.space_dims >= 2
            v2quad = data.Dp1list(k,3);
        else
            v2quad = 1;
        end
        if data.space_dims >= 3
            v3quad = data.Dp1list(k,4);
        else
            v3quad = 1;
        end
        weight = data.Dp1quadwgts(k);
        psil = data.vectpsi(:,:,tquad,v1quad,v2quad,v3quad);
        psiktau = data.vectpsi_dtau_Trans(:,:,tquad,v1quad,v2quad,v3quad);
        temp3 = temp3 + psiktau*psil*weight;
    end        
    data.I_futr = temp1;                
    data.I_past = temp2;
    Psi_dtau = temp3;
    data.tauflux = data.I_futr-data.I_past-Psi_dtau;                


    projection_temp = 0;
    PHI_norm_temp = 0;
    PSI_norm_temp = 0;

    for v1quad = 1:data.n1quad
    for v2quad = 1:data.n2quad
    for v3quad = 1:data.n3quad
        for tquad = 1:data.n1quad
            weight = data.wgts_spacetime(tquad,v1quad,v2quad,v3quad);
            psitrans = data.vectpsi_Trans(:,:,tquad,v1quad,v2quad,v3quad);
            psi = data.vectpsi(:,:,tquad,v1quad,v2quad,v3quad);
            phi = data.vectphi(:,:,v1quad,v2quad,v3quad);

            projection_temp = projection_temp + psitrans*phi*weight;
            PSI_norm_temp = PSI_norm_temp + psitrans*psi*weight;
        end
    end
    end
    end

    for v1quad = 1:data.n1quad
    for v2quad = 1:data.n2quad
    for v3quad = 1:data.n3quad
            weight = data.wgts_space(v1quad,v2quad,v3quad);
            phitrans = data.vectphi_Trans(:,:,v1quad,v2quad,v3quad);
            phi = data.vectphi(:,:,v1quad,v2quad,v3quad);
            PHI_norm_temp = PHI_norm_temp + phitrans*phi*weight;
    end
    end
    end

    PHI_norm = PHI_norm_temp;
    PSI_norm = PSI_norm_temp;
    PHI_norm_inv = inv(PHI_norm_temp);
    PSI_norm_inv = inv(PSI_norm_temp);
    PHI2PSI = PSI_norm_inv*projection_temp;


    data.extrapolate_psi_x = 0;
    data.extrapolate_psi_y = 0;
    for k = 1:data.Pdp1
        tquad = data.Dp1list(k,1); 
        v1quad = data.Dp1list(k,2); 
        p = max(data.Dp1list(:,2));
        if data.space_dims >= 2
            v2quad = data.Dp1list(k,3);
        else
            v2quad = 1;
        end
        if data.space_dims >= 3
            v3quad = data.Dp1list(k,4);
        else
            v3quad = 1;
        end    
        weight = data.Dp1quadwgts(k);
        psiT = data.vectpsi_Trans(:,:,tquad,v1quad,v2quad,v3quad);
        psi = data.vectpsi(:,:,tquad,p+1-v1quad,v2quad,v3quad);
        data.extrapolate_psi_x = data.extrapolate_psi_x + weight*psiT*psi;
        if data.space_dims >= 2
            psi = data.vectpsi(:,:,tquad,v1quad,data.P+1-v2quad,v3quad);
            data.extrapolate_psi_y = data.extrapolate_psi_y + weight*psiT*psi;
        end
    end
    data.extrapolate_psi_x = PSI_norm_inv*data.extrapolate_psi_x;
    if data.space_dims >= 2
        data.extrapolate_psi_y = PSI_norm_inv*data.extrapolate_psi_y;
    end
end
