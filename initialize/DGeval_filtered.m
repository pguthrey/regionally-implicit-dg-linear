function [ q ] = DGeval_filtered(DGcoeffs,quadpoint,data)

switch data.space_dims 
    case 1
        q = DGeval_1D_filtered(DGcoeffs,quadpoint,data);  
    otherwise
        error('program more cases')
end
