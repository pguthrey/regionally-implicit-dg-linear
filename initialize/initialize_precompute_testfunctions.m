function [data] = initialize_precompute_testfunctions(data)
% Precomputes the test functions at quadrature points
% written by Pierson Guthrey
% -------------------------------------------------
% INPUTS    
% OUTPUTS   Precompute test funtions
% Note: other variables may be %loaded in from the problem parameter files
% ------------------------------------------------------------------------

%h = waitbar(0,'Precomputing Test Functions','OuterPosition', [100 100 300 75]);
Neqns = data.Neqns;
P = data.P;
Pt = P;
Px = P;
Py = 1;
Pz = 1;
if data.space_dims >= 2
    Py = P;
    if data.space_dims >= 3
        Pz = P;
    end
end

data.locs = data.locs;
thetaT = data.thetaT;
theta = data.theta;
sysBO = data.sysBO;
sysBOcoeffs = data.sysBOcoeffs;
sysBOs = data.sysBOs;
sysBOscoeffs = data.sysBOscoeffs;
 
vectpsi = zeros(Neqns,thetaT,Pt,Px,Py,Pz);
vectphi = zeros(Neqns,theta,Px,Py,Pz);
vectpsi_Trans = zeros(thetaT,Neqns,Pt,Px,Py,Pz);
vectphi_Trans = zeros(theta,Neqns,Px,Py,Pz);
vectphiplot = zeros(Neqns,theta,Px,Py,Pz);

syms xii real   
test = sym(zeros(1,data.M));
test_diff = sym(zeros(1,data.M));

testquad = zeros(1,data.M,length(data.locs));
for iell = 1:data.M
for iloc = 1:length(data.locs)
    testquad(1,iell,iloc) = testfunction_Legendre(data.locs(iloc),iell);    
end
end

Zphi = 1;
Yphi = 1;

for kay = 1:theta
    for eqn = 1:Neqns
    ells = sysBOs(eqn,kay);
    coeffs = sysBOscoeffs(eqn,kay);
    for k = 1:data.Pd
        v1quad = data.Dlist(k,1); 
        if data.space_dims >= 2
            v2quad = data.Dlist(k,2);
        else
            v2quad = 1;
        end
        if data.space_dims >= 3
            v3quad = data.Dlist(k,3);
        else
            v3quad = 1;
        end
        Xphi = testquad(1,data.BOs(ells,1),v1quad);
        if data.space_dims >= 2
            Yphi = testquad(1,data.BOs(ells,2),v2quad);
            if data.space_dims >= 3
                Zphi = testquad(1,data.BOs(ells,3),v3quad);
            end
        end
        %Phi terms
        vectphi(eqn,kay,v1quad,v2quad,v3quad) =  Xphi.*Yphi.*Zphi.*coeffs;
        vectphi_Trans(kay,eqn,v1quad,v2quad,v3quad) =  vectphi(eqn,kay,v1quad,v2quad,v3quad);
    end
    end
end
Zpsi = 1;
Ypsi = 1;

for kay = 1:thetaT
        for eqn = 1:Neqns
        ell = sysBO(eqn,kay);
        coeffs = sysBOcoeffs(eqn,kay);
        for k = 1:data.Pdp1
            tquad = data.Dp1list(k,1); 
            v1quad = data.Dp1list(k,2); 
            if data.space_dims >= 2
                v2quad = data.Dp1list(k,3);
            else
                v2quad = 1;
            end
            if data.space_dims >= 3
                v3quad = data.Dp1list(k,4);
            else
                v3quad = 1;
            end 
        Tpsi = testquad(1,data.BO(ell,1),tquad);
        Xpsi = testquad(1,data.BO(ell,2),v1quad);
        if data.space_dims >= 2
            Ypsi = testquad(1,data.BO(ell,3),v2quad);
            if data.space_dims >= 3
                Zpsi = testquad(1,data.BO(ell,4),v3quad);
            end
        end
        
        vectpsi(eqn,kay,tquad,v1quad,v2quad,v3quad) =  Tpsi.*Xpsi.*Ypsi.*Zpsi.*coeffs;
        vectpsi_Trans(kay,eqn,tquad,v1quad,v2quad,v3quad) =  vectpsi(eqn,kay,tquad,v1quad,v2quad,v3quad);
        end
        end
end

%{
for kay = 1:theta
    for eqn = 1:Neqns
    ells = sysBOs(eqn,kay);
    coeffs = sysBOscoeffs(eqn,kay);
    for k = 1:data.Pdplot
        v1quad = data.plotlist(k,1); 
        v1loc = data.Dplotlocs(k,1);
        if data.space_dims >= 2
            v2quad = data.plotlist(k,2);
            v2loc = data.Dplotlocs(k,2);
        else
            v2quad = 1;
            v2loc = inf;
        end
        if data.space_dims >= 3
            v3quad = data.plotlist(k,3);
            v3loc = data.Dplotlocs(k,3);
        else
            v3quad = 1;
            v3loc = inf;
        end
        quadpoint = [v1loc,v2loc,v3loc];
        %Phi terms
        vectphiplot(eqn,kay,v1quad,v2quad,v3quad) =  testfunction_phi(quadpoint,ells,data).*coeffs;        
    end
    end
end  
%}

data.vectpsi = vectpsi ;
data.vectpsi_Trans = vectpsi_Trans; 
data.vectphi = vectphi;
data.vectphi_Trans = vectphi_Trans;
data.vectphiplot = vectphiplot;

warning('set this value')
if ~strcmp(data.linearcase,'constantcoefficient')
  

    vectpsi_dxii = zeros(Neqns,thetaT,Pt,Px,Py,Pz);
    vectpsi_west = zeros(Neqns,thetaT,Px,Py,Pz);
    vectpsi_east = zeros(Neqns,thetaT,Px,Py,Pz);
    vectpsi_dxii_Trans = zeros(thetaT,Neqns,Px,Py,Pz);
    vectpsi_futr = zeros(Neqns,thetaT,Px,Py,Pz);
    vectpsi_dtau_Trans = zeros(thetaT,Neqns,Px,Py,Pz);
    vectpsi_futr_Trans = zeros(thetaT,Neqns,Px,Py,Pz);
    vectpsi_past_Trans = zeros(thetaT,Neqns,Px,Py,Pz);
    vectpsi_west_Trans = zeros(thetaT,Neqns,Px,Py,Pz);
    vectpsi_east_Trans = zeros(thetaT,Neqns,Px,Py,Pz);
    vectphi_dxii = zeros(Neqns,theta,Px,Py,Pz);
    vectphi_dxii_Trans = zeros(theta,Neqns,Px,Py,Pz);
    vectphi_west = zeros(Neqns,theta,Py,Pz);
    vectphi_east = zeros(Neqns,theta,Py,Pz);
    vectphi_west_Trans = zeros(theta,Neqns,Py,Pz);
    vectphi_east_Trans = zeros(theta,Neqns,Py,Pz);


    vectpsi_deta = zeros(Neqns,thetaT,Pt,Px,Py,Pz);
    vectpsi_nort = zeros(Neqns,thetaT,Px,Py,Pz);
    vectpsi_sout = zeros(Neqns,thetaT,Px,Py,Pz);
    vectpsi_deta_Trans = zeros(thetaT,Neqns,Px,Py,Pz);
    vectpsi_nort_Trans = zeros(thetaT,Neqns,Px,Py,Pz);
    vectpsi_sout_Trans = zeros(thetaT,Neqns,Px,Py,Pz);
    vectphi_deta = zeros(Neqns,theta,Px,Py,Pz);
    vectphi_deta_Trans = zeros(theta,Neqns,Px,Py,Pz);
    vectphi_nort = zeros(Neqns,theta,Py,Pz);
    vectphi_sout = zeros(Neqns,theta,Py,Pz);
    vectphi_nort_Trans = zeros(theta,Neqns,Py,Pz);
    vectphi_sout_Trans = zeros(theta,Neqns,Py,Pz);

    vectpsi_uppr = zeros(Neqns,thetaT,Px,Py,Pz);
    vectpsi_down = zeros(Neqns,thetaT,Px,Py,Pz);
    vectpsi_dzta = zeros(Neqns,thetaT,Pt,Px,Py,Pz);
    vectpsi_dzta_Trans = zeros(thetaT,Neqns,Px,Py,Pz);
    vectpsi_uppr_Trans = zeros(thetaT,Neqns,Px,Py,Pz);
    vectpsi_down_Trans = zeros(thetaT,Neqns,Px,Py,Pz);
    vectphi_dzta = zeros(Neqns,theta,Px,Py,Pz);
    vectphi_dzta_Trans = zeros(theta,Neqns,Px,Py,Pz);
    vectphi_uppr = zeros(Neqns,theta,Py,Pz);
    vectphi_down = zeros(Neqns,theta,Py,Pz);
    vectphi_uppr_Trans = zeros(theta,Neqns,Py,Pz);
    vectphi_down_Trans = zeros(theta,Neqns,Py,Pz);

    for kay = 1:thetaT
        for eqn = 1:Neqns
        ell = sysBO(eqn,kay);
        coeff = sysBOcoeffs(eqn,kay);

        for k = 1:data.Pd
            v1quad = data.Dlist(k,1); 
            v1loc = data.Dquadlocs(k,1);
            if data.space_dims >= 2
                v2quad = data.Dlist(k,2);
                v2loc = data.Dquadlocs(k,2);
            else
                v2quad = 1;
                v2loc = inf;
            end
            if data.space_dims >= 3
                v3quad = data.Dlist(k,3);
                v3loc = data.Dquadlocs(k,3);
            else
                v3quad = 1;
                v3loc = inf;
            end
            vectpsi_west(eqn,kay,v1quad,v2quad,v3quad) = testfunction_psi([v1loc,-1,v2loc,v3loc],ell,data).*coeff;
            vectpsi_east(eqn,kay,v1quad,v2quad,v3quad) = testfunction_psi([v1loc, 1,v2loc,v3loc],ell,data).*coeff;
            vectpsi_west_Trans(kay,eqn,v1quad,v2quad,v3quad) = testfunction_psi([v1loc,-1,v2loc,v3loc],ell,data).*coeff;
            vectpsi_east_Trans(kay,eqn,v1quad,v2quad,v3quad) = testfunction_psi([v1loc, 1,v2loc,v3loc],ell,data).*coeff;       
            if data.space_dims >= 2
                vectpsi_nort(eqn,kay,v1quad,v2quad,v3quad) = testfunction_psi([v1loc,v2loc, 1,v3loc],ell,data).*coeff;
                vectpsi_sout(eqn,kay,v1quad,v2quad,v3quad) = testfunction_psi([v1loc,v2loc,-1,v3loc],ell,data).*coeff;
                vectpsi_nort_Trans(kay,eqn,v1quad,v2quad,v3quad) = testfunction_psi([v1loc,v2loc, 1,v3loc],ell,data).*coeff;
                vectpsi_sout_Trans(kay,eqn,v1quad,v2quad,v3quad) = testfunction_psi([v1loc,v2loc,-1,v3loc],ell,data).*coeff;            
                if data.space_dims >= 3
                    vectpsi_uppr(eqn,kay,v1quad,v2quad,v3quad) = testfunction_psi([v1loc,v2loc,v3loc,1],ell,data).*coeff;
                    vectpsi_down(eqn,kay,v1quad,v2quad,v3quad) = testfunction_psi([v1loc,v2loc,v3loc,-1],ell,data).*coeff;
                    vectpsi_uppr_Trans(kay,eqn,v1quad,v2quad,v3quad) = testfunction_psi([v1loc,v2loc,v3loc,1],ell,data).*coeff;
                    vectpsi_down_Trans(kay,eqn,v1quad,v2quad,v3quad) = testfunction_psi([v1loc,v2loc,v3loc,-1],ell,data).*coeff;
                end
            end
            vectpsi_futr(eqn,kay,v1quad,v2quad,v3quad) =  testfunction_psi([1,v1loc,v2loc,v3loc],ell,data).*coeff;
            vectpsi_futr_Trans(kay,eqn,v1quad,v2quad,v3quad) =  testfunction_psi([1,v1loc,v2loc,v3loc],ell,data).*coeff;
            vectpsi_past_Trans(kay,eqn,v1quad,v2quad,v3quad) =  testfunction_psi([-1,v1loc,v2loc,v3loc],ell,data).*coeff;
        end
        end
    end

    clear v1quad v2quad v3quad v1loc v2loc v3loc tquad tloc ell coeff ells coeffs quadpoint

    %Phi Terms
    for kay = 1:theta
        for eqn = 1:Neqns
        ells = sysBOs(eqn,kay);
        coeffs = sysBOscoeffs(eqn,kay);
        for k = 1:data.Pdm1
            if data.space_dims >= 2
                v1quad = data.Dm1list(k,1);
                v1loc = data.Dm1quadlocs(k,1);
            else
                v1quad = 1;
                v1loc = inf;
            end
            if data.space_dims >= 3
                v2quad = data.Dm1list(k,2);
                v2loc = data.Dm1quadlocs(k,2);
            else
                v2quad = 1;
                v2loc = inf;
            end
            vectphi_east(eqn,kay,v1quad,v2quad) =  testfunction_phi([1,v1loc,v2loc],ells,data).*coeffs;
            vectphi_east_Trans(kay,eqn,v1quad,v2quad) =  testfunction_phi([1,v1loc,v2loc],ells,data).*coeffs;        
            vectphi_west(eqn,kay,v1quad,v2quad) =  testfunction_phi([-1,v1loc,v2loc],ells,data).*coeffs;
            vectphi_west_Trans(kay,eqn,v1quad,v2quad) =  testfunction_phi([-1,v1loc,v2loc],ells,data).*coeffs;
            if data.space_dims >= 2
                vectphi_nort(eqn,kay,v1quad,v2quad) =  testfunction_phi([v1loc,1,v2loc],ells,data).*coeffs;
                vectphi_nort_Trans(kay,eqn,v1quad,v2quad) =  testfunction_phi([v1loc,1,v2loc],ells,data).*coeffs;        
                vectphi_sout(eqn,kay,v1quad,v2quad) =  testfunction_phi([v1loc,-1,v2loc],ells,data).*coeffs;
                vectphi_sout_Trans(kay,eqn,v1quad,v2quad) =  testfunction_phi([v1loc,-1,v2loc],ells,data).*coeffs;
                if data.space_dims >= 3
                    vectphi_uppr(eqn,kay,v1quad,v2quad) =  testfunction_phi([v1loc,v2loc,1],ells,data).*coeffs;
                    vectphi_uppr_Trans(kay,eqn,v1quad,v2quad) =  testfunction_phi([v1loc,v2loc,1],ells,data).*coeffs;        
                    vectphi_down(eqn,kay,v1quad,v2quad) =  testfunction_phi([v1loc,v2loc,-1],ells,data).*coeffs;
                    vectphi_down_Trans(kay,eqn,v1quad,v2quad) =  testfunction_phi([v1loc,v2loc,-1],ells,data).*coeffs;
                end
            end
        end
        end
    end

    clear v1quad v2quad v3quad v1loc v2loc v3loc tquad tloc ell coeff ells coeffs quadpoint

    for kay = 1:theta
        for eqn = 1:Neqns
        ells = sysBOs(eqn,kay);
        coeffs = sysBOscoeffs(eqn,kay);
        for k = 1:data.Pd
            v1quad = data.Dlist(k,1); 
            v1loc = data.Dquadlocs(k,1);
            if data.space_dims >= 2
                v2quad = data.Dlist(k,2);
                v2loc = data.Dquadlocs(k,2);
            else
                v2quad = 1;
                v2loc = inf;
            end
            if data.space_dims >= 3
                v3quad = data.Dlist(k,3);
                v3loc = data.Dquadlocs(k,3);
            else
                v3quad = 1;
                v3loc = inf;
            end
            quadpoint = [v1loc,v2loc,v3loc];
            %Phi terms
            vectphi(eqn,kay,v1quad,v2quad,v3quad) =  testfunction_phi(quadpoint,ells,data).*coeffs;
            vectphi_Trans(kay,eqn,v1quad,v2quad,v3quad) =  testfunction_phi(quadpoint,ells,data).*coeffs;
            vectphi_dxii(eqn,kay,v1quad,v2quad,v3quad) = testfunction_phi_dxii(quadpoint,ells,data).*coeffs;
            if data.space_dims >= 2
                vectphi_deta(eqn,kay,v1quad,v2quad,v3quad) = testfunction_phi_deta(quadpoint,ells,data).*coeffs;
                if data.space_dims >= 3
                    vectphi_dzta(eqn,kay,v1quad,v2quad,v3quad) = testfunction_phi_dzta(quadpoint,ells,data).*coeffs;
                end
            end
        end
        end
    end
 
    clear v1quad v2quad v3quad v1loc v2loc v3loc tquad tloc ell coeff ells coeffs quadpoint


    %Phi derivative terms
    for kay = 1:theta
        for eqn = 1:Neqns
        ells = sysBOs(eqn,kay);
        coeffs = sysBOscoeffs(eqn,kay);
        for k = 1:data.Pd
            v1quad = data.Dlist(k,1); 
            v1loc = data.Dquadlocs(k,1);
            if data.space_dims >= 2
                v2quad = data.Dlist(k,2);
                v2loc = data.Dquadlocs(k,2);
            else
                v2quad = 1;
                v2loc = inf;
            end
            if data.space_dims >= 3
                v3quad = data.Dlist(k,3);
                v3loc = data.Dquadlocs(k,3);
            else
                v3quad = 1;
                v3loc = inf;
            end
            quadpoint = [v1loc,v2loc,v3loc];
            %Phi terms
            vectphi_dxii(eqn,kay,v1quad,v2quad,v3quad) = testfunction_phi_dxii(quadpoint,ells,data).*coeffs;
            vectphi_dxii_Trans(kay,eqn,v1quad,v2quad,v3quad) = testfunction_phi_dxii(quadpoint,ells,data).*coeffs;
            if data.space_dims >= 2
                vectphi_deta(eqn,kay,v1quad,v2quad,v3quad) = testfunction_phi_deta(quadpoint,ells,data).*coeffs;
                vectphi_deta_Trans(kay,eqn,v1quad,v2quad,v3quad) = testfunction_phi_deta(quadpoint,ells,data).*coeffs;
                if data.space_dims >= 3
                    vectphi_dzta(eqn,kay,v1quad,v2quad,v3quad) = testfunction_phi_dzta(quadpoint,ells,data).*coeffs;
                    vectphi_dzta_Trans(kay,eqn,v1quad,v2quad,v3quad) = testfunction_phi_dzta(quadpoint,ells,data).*coeffs;
                end
            end
        end
        end
    end

    clear v1quad v2quad v3quad v1loc v2loc v3loc tquad tloc ell coeff ells coeffs quadpoint

    for kay = 1:thetaT
        for eqn = 1:Neqns
        ell = sysBO(eqn,kay);
        coeff = sysBOcoeffs(eqn,kay);
        for k = 1:data.Pdp1
            tquad = data.Dp1list(k,1); 
            tloc = data.Dp1quadlocs(k,1);
            v1quad = data.Dp1list(k,2); 
            v1loc = data.Dp1quadlocs(k,2);
            if data.space_dims >= 2
                v2quad = data.Dp1list(k,3);
                v2loc = data.Dp1quadlocs(k,3);
            else
                v2quad = 1;
                v2loc = inf;
            end
            if data.space_dims >= 3
                v3quad = data.Dp1list(k,4);
                v3loc = data.Dp1quadlocs(k,4);
            else
                v3quad = 1;
                v3loc = inf;
            end
            quadpoint = [tloc,v1loc,v2loc,v3loc];
            vectpsi_dtau_Trans(kay,eqn,tquad,v1quad,v2quad,v3quad) = testfunction_psi_dtau(quadpoint,ell,data).*coeff;
            vectpsi_dxii(eqn,kay,tquad,v1quad,v2quad,v3quad) = testfunction_psi_dxii(quadpoint,ell,data).*coeff;
            vectpsi_dxii_Trans(kay,eqn,tquad,v1quad,v2quad,v3quad) = testfunction_psi_dxii(quadpoint,ell,data).*coeff;
            if data.space_dims >= 2
                vectpsi_deta_Trans(kay,eqn,tquad,v1quad,v2quad,v3quad) = testfunction_psi_deta(quadpoint,ell,data).*coeff;         
                vectpsi_deta(eqn,kay,tquad,v1quad,v2quad,v3quad) = testfunction_psi_deta(quadpoint,ell,data).*coeff;
                if data.space_dims >= 3
                vectpsi_dzta(eqn,kay,tquad,v1quad,v2quad,v3quad) = testfunction_psi_dzta(quadpoint,ell,data).*coeff;
                vectpsi_dzta_Trans(kay,eqn,tquad,v1quad,v2quad,v3quad) = testfunction_psi_dzta(quadpoint,ell,data).*coeff;
                end
            end        
        end
        end
    end
    clear v1quad v2quad v3quad v1loc v2loc v3loc tquad tloc ell coeff ells coeffs quadpoint

    for kay = 1:theta
        for eqn = 1:Neqns
        ells = sysBOs(eqn,kay);
        coeffs = sysBOscoeffs(eqn,kay);
        for k = 1:data.Pdlimiter
            v1quad = data.limiterlist(k,1); 
            v1loc = data.Dlimiterlocs(k,1);
            if data.space_dims >= 2
                v2quad = data.limiterlist(k,2);
                v2loc = data.Dlimiterlocs(k,2);
            else
                v2quad = 1;
                v2loc = inf;
            end
            if data.space_dims >= 3
                v3quad = data.limiterlist(k,3);
                v3loc = data.Dlimiterlocs(k,3);
            else
                v3quad = 1;
                v3loc = inf;
            end
            quadpoint = [v1loc,v2loc,v3loc];
            %Phi terms
            data.vectphi_limiter(eqn,kay,v1quad,v2quad,v3quad) =  testfunction_phi(quadpoint,ells,data).*coeffs;        
        end
        end
    end

    for kay = 1:thetaT
        for eqn = 1:Neqns
        ells = sysBO(eqn,kay);
        coeffs = sysBOcoeffs(eqn,kay);
        for tquad = 1:data.P+2
        for v1quad = 1:data.P+2
        for v2quad = 1:data.P+2        
            tloc = data.limiterlocs(tquad);
            v1loc = data.limiterlocs(v1quad);
            v2loc = data.limiterlocs(v2quad);
            v3loc = inf;
            quadpoint = [tloc,v1loc,v2loc,v3loc];
            data.vectpsi_limiter(eqn,kay,tquad,v1quad,v2quad,v3quad) =  testfunction_psi(quadpoint,ells,data).*coeffs;        
        end
        end
        end
        end
    end
    
    data.vectpsi_dxii = vectpsi_dxii ;
    data.vectpsi_futr = vectpsi_futr;
    data.vectpsi_west = vectpsi_west;
    data.vectpsi_east = vectpsi_east;
    data.vectpsi_dxii_Trans = vectpsi_dxii_Trans;
    data.vectpsi_futr_Trans = vectpsi_futr_Trans;
    data.vectpsi_past_Trans = vectpsi_past_Trans;
    data.vectpsi_west_Trans = vectpsi_west_Trans;
    data.vectpsi_east_Trans = vectpsi_east_Trans;
    data.vectphi_dxii = vectphi_dxii;
    data.vectphi_dxii_Trans = vectphi_dxii_Trans;
    data.vectphi_west = vectphi_west;
    data.vectphi_east = vectphi_east;
    data.vectphi_west_Trans = vectphi_west_Trans;
    data.vectphi_east_Trans = vectphi_east_Trans;
    data.vectpsi_dtau_Trans = vectpsi_dtau_Trans;

    if data.space_dims >= 2 
        data.vectpsi_deta = vectpsi_deta;
        data.vectpsi_nort = vectpsi_nort;
        data.vectpsi_sout = vectpsi_sout;
        data.vectpsi_deta_Trans = vectpsi_deta_Trans;
        data.vectpsi_nort_Trans = vectpsi_nort_Trans;
        data.vectpsi_sout_Trans = vectpsi_sout_Trans;
        data.vectphi_nort = vectphi_nort;
        data.vectphi_sout = vectphi_sout;
        data.vectphi_nort_Trans = vectphi_nort_Trans;
        data.vectphi_sout_Trans = vectphi_sout_Trans;
        data.vectphi_deta = vectphi_deta;    
        data.vectphi_deta_Trans = vectphi_deta_Trans;
        if data.space_dims >= 3
            data.vectpsi_dzta = vectpsi_dzta;
            data.vectpsi_uppr = vectpsi_uppr;
            data.vectpsi_down = vectpsi_down;
            data.vectpsi_dzta_Trans = vectpsi_dzta_Trans;
            data.vectpsi_uppr_Trans = vectpsi_uppr_Trans;
            data.vectpsi_down_Trans = vectpsi_down_Trans;
            data.vectphi_dzta = vectphi_dzta;
            data.vectphi_dzta_Trans = vectphi_dzta_Trans;
            data.vectphi_uppr = vectphi_uppr;
            data.vectphi_down = vectphi_down;
            data.vectphi_uppr_Trans = vectphi_uppr_Trans;
            data.vectphi_down_Trans = vectphi_down_Trans;
        end
    end

    data.bases = zeros(data.thetaT,data.theta,Pt,Px,Py,Pz);

    for kay = 1:thetaT
        for eqn = 1:Neqns
        ells = sysBO(eqn,kay);
        coeffs = sysBOcoeffs(eqn,kay);
        for k = 1:data.Pdplotpsi
            tquad = data.plotlistpsi(k,1); 
            tloc = data.plotlistpsi(k,1); 
            v1quad = data.plotlistpsi(k,2); 
            v1loc = data.Dplotlocspsi(k,2);
            if data.space_dims >= 2
                v2quad = data.plotlistpsi(k,3);
                v2loc = data.Dplotlocspsi(k,3);
            else
                v2quad = 1;
                v2loc = inf;
            end
            if data.space_dims >= 3
                v3quad = data.plotlistpsi(k,4);
                v3loc = data.Dplotlocspsi(k,4);
            else
                v3quad = 1;
                v3loc = inf;
            end
            quadpoint = [tloc,v1loc,v2loc,v3loc];
            %Phi terms
            data.vectpsiplot(eqn,kay,tquad,v1quad,v2quad,v3quad) =  testfunction_psi(quadpoint,ells,data).*coeffs;        
        end
        end
    end

end
