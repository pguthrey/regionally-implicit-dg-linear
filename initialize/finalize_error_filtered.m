function [data] = finalize_error_filtered(DGsoln,data)

solution = @(quadpoint) problem_solution(data.Tfinal,quadpoint,data);    

L1err_norm_cell = NaN(data.Nv1,data.Nv2,data.Nv3);
L2err_norm_cell = NaN(data.Nv1,data.Nv2,data.Nv3);
Lierr_norm_cell = NaN(data.Nv1,data.Nv2,data.Nv3);
L1err_norm_filtered_cell = NaN(data.Nv1,data.Nv2,data.Nv3);
L2err_norm_filtered_cell = NaN(data.Nv1,data.Nv2,data.Nv3);
Lierr_norm_filtered_cell = NaN(data.Nv1,data.Nv2,data.Nv3);      
L1exact_norm_cell = NaN(data.Nv1,data.Nv2,data.Nv3);
L2exact_norm_cell = NaN(data.Nv1,data.Nv2,data.Nv3);
Liexact_norm_cell = NaN(data.Nv1,data.Nv2,data.Nv3);

filtername = ['filterdata/filterdata_' num2str(data.space_dims) 'D_M' num2str(data.M) '_' data.correctorbasis 'basis'];
load(filtername)

data.DGxcells = DGxcells;
if data.space_dims >= 2
    data.DGycells = DGycells;
end

h = waitbar(0,'Filtering');
for iv1 = 1:data.Nv1
for iv2 = 1:data.Nv2
for iv3 = 1:data.Nv3
    v1bar = data.v1centers(iv1);
    v2bar = data.v1centers(iv2);
    v3bar = data.v1centers(iv3);

    quadlocsx = v1bar + data.deltav1/2*quadlocs;
    quadlocsy = NaN;
    quadlocsz = NaN;
    if data.space_dims >= 2
        quadlocsy = v2bar + data.deltav2/2*quadlocs;
        if data.space_dims >= 3
            quadlocsz = v3bar + data.deltav3/2*quadlocs;
        end
    end
    weightsx = quadwgts*data.deltav1;
    weightsy = 1;
    weightsz = 1;
    if data.space_dims >= 2
        weightsy = quadwgts*data.deltav2;
        if data.space_dims >= 3
            weightsz = quadwgts*data.deltav3;
        end
    end    

    weight = NaN(length(quadlocsx),length(quadlocsy),length(quadlocsz));
    approx = NaN(length(quadlocsx),length(quadlocsy),length(quadlocsz));
    filtered  = NaN(length(quadlocsx),length(quadlocsy),length(quadlocsz));
    exact = NaN(length(quadlocsx),length(quadlocsy),length(quadlocsz));
    for iq1 = 1:length(quadlocsx)
    for iq2 = 1:length(quadlocsy)
    for iq3 = 1:length(quadlocsz)
        quadpoint = [quadlocsx(iq1) quadlocsy(iq2) quadlocsz(iq3)];
                     
        weight(iq1,iq2,iq3) = weightsx(iq1)*weightsy(iq2)*weightsz(iq3);         
        approx(iq1,iq2,iq3) =  DGeval(DGsoln,quadpoint,data);
        switch data.space_dims
            case 1        
                data.thisfilter = quadlocsfilter(:,:,:,iq1);
                filtered(iq1,iq2,iq3) = my1Dfilter(DGsoln,iv1,data);
            case 2
                data.thisfilter = quadlocsfilter(:,:,:,:,iq1,iq2);
                filtered(iq1,iq2,iq3) = my2Dfilter(DGsoln,iv1,iv2,data);
        end
        exact(iq1,iq2,iq3) = solution(quadpoint);        
    end
    end
    end
    error = abs(approx-exact);
    errorfiltered = abs(filtered-exact);
    exactnorm = abs(exact);
    
    L1err_norm = sum(sum(sum(error.*weight)));
    L2err_norm = sum(sum(sum(error.^2.*weight)));
    Lierr_norm = max(max(max(error)));
    L1err_norm_filtered = sum(sum(sum(errorfiltered.*weight)));
    L2err_norm_filtered = sum(sum(sum(errorfiltered.^2.*weight)));
    Lierr_norm_filtered = max(max(max(errorfiltered)));        
    L1exact_norm = sum(sum(sum(exactnorm.*weight)));
    L2exact_norm = sum(sum(sum(exactnorm.^2.*weight)));
    Liexact_norm = max(max(max(exactnorm)));

    L1err_norm_cell(iv1,iv2,iv3) = L1err_norm;
    L2err_norm_cell(iv1,iv2,iv3) = L2err_norm;
    Lierr_norm_cell(iv1,iv2,iv3) = Lierr_norm;
    L1err_norm_filtered_cell(iv1,iv2,iv3) = L1err_norm_filtered;
    L2err_norm_filtered_cell(iv1,iv2,iv3) = L2err_norm_filtered;
    Lierr_norm_filtered_cell(iv1,iv2,iv3) = Lierr_norm_filtered;        
    L1exact_norm_cell(iv1,iv2,iv3) = L1exact_norm;
    L2exact_norm_cell(iv1,iv2,iv3) = L2exact_norm;
    Liexact_norm_cell(iv1,iv2,iv3) = Liexact_norm;
    
    done = (iv2+(iv1-1)*data.Nv2)/(data.Nv1*data.Nv2);
    waitbar(done,h,'Filtering')        
end
end
end
delete(h)

L1err_norm_total = sum(sum(sum(L1err_norm_cell)));
L2err_norm_total = sqrt(sum(sum(sum(L2err_norm_cell))));
Lierr_norm_total = max(max(max(Lierr_norm_cell)));
L1err_norm_filtered_total = sum(sum(sum(L1err_norm_filtered_cell)));
L2err_norm_filtered_total = sqrt(sum(sum(sum(L2err_norm_filtered_cell))));
Lierr_norm_filtered_total = max(max(max(Lierr_norm_filtered_cell))); 
L1exact_norm_total = sum(sum(sum(L1exact_norm_cell)));
L2exact_norm_total = sqrt(sum(sum(sum(L2exact_norm_cell))));
Liexact_norm_total = max(max(max(Liexact_norm_cell)));

Rel_L1err = L1err_norm_total/L1exact_norm_total;
Rel_L2err = L2err_norm_total/L2exact_norm_total;
Rel_Lierr = Lierr_norm_total/Liexact_norm_total;    

Rel_L1err_filtered = L1err_norm_filtered_total/L1exact_norm_total;
Rel_L2err_filtered = L2err_norm_filtered_total/L2exact_norm_total;
Rel_Lierr_filtered = Lierr_norm_filtered_total/Liexact_norm_total;    

data.Rel_L1err = Rel_L1err;
data.Rel_L2err = Rel_L2err;
data.Rel_Lierr = Rel_Lierr;

data.Rel_L1err_filtered = Rel_L1err_filtered;
data.Rel_L2err_filtered = Rel_L2err_filtered;
data.Rel_Lierr_filtered = Rel_Lierr_filtered;