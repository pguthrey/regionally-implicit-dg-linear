function [data] = finalize_error(DGsoln,data)

solution = @(quadpoint) problem_solution(data.Tfinal,quadpoint,data);    

L1err_norm_cell = NaN(data.Nv1,data.Nv2,data.Nv3);
L2err_norm_cell = NaN(data.Nv1,data.Nv2,data.Nv3);
Lierr_norm_cell = NaN(data.Nv1,data.Nv2,data.Nv3);
L1exact_norm_cell = NaN(data.Nv1,data.Nv2,data.Nv3);
L2exact_norm_cell = NaN(data.Nv1,data.Nv2,data.Nv3);
Liexact_norm_cell = NaN(data.Nv1,data.Nv2,data.Nv3);

data = initialize_gauss_lobatto_quadrature(data,data.M+3);
quadlocs = data.locs;
quadwgts = data.wgts1D;

testquad = zeros(1,data.M,length(quadlocs));
for iell = 1:data.M
for iloc = 1:length(quadlocs)
    testquad(1,iell,iloc) = testfunction_Legendre(quadlocs(iloc),iell);    
end
end

Zphi = 1;
Yphi = 1;

vectphi_err = zeros(1,data.theta,length(quadlocs),length(quadlocs),length(quadlocs));
for kay = 1:data.theta
    for eqn = 1:data.Neqns
    ells = data.sysBOs(eqn,kay);
    coeffs = data.sysBOscoeffs(eqn,kay);
        for v1quad = 1:length(quadlocs)
        for v2quad = 1:length(quadlocs)
        for v3quad = 1:length(quadlocs)
        Xphi = testquad(1,data.BOs(ells,1),v1quad);
        if data.space_dims >= 2
            Yphi = testquad(1,data.BOs(ells,2),v2quad);
            if data.space_dims >= 3
            Zphi = testquad(1,data.BOs(ells,3),v3quad);
            end
        end
        vectphi_err(eqn,kay,v1quad,v2quad,v3quad) =  Xphi.*Yphi.*Zphi.*coeffs;
        end
        end
        end
    end
end

for iv1 = 1:data.Nv1
for iv2 = 1:data.Nv2
for iv3 = 1:data.Nv3
    v1bar = data.v1centers(iv1);
    v2bar = data.v1centers(iv2);
    v3bar = data.v1centers(iv3);

    quadlocsx = v1bar + data.deltav1/2*quadlocs;
    quadlocsy = NaN;
    quadlocsz = NaN;
    if data.space_dims >= 2
        quadlocsy = v2bar + data.deltav2/2*quadlocs;
        if data.space_dims >= 3
            quadlocsz = v3bar + data.deltav3/2*quadlocs;
        end
    end
    weightsx = quadwgts*data.deltav1;
    weightsy = 1;
    weightsz = 1;
    if data.space_dims >= 2
        weightsy = quadwgts*data.deltav2;
        if data.space_dims >= 3
            weightsz = quadwgts*data.deltav3;
        end
    end    
    weight = NaN(length(quadlocsx),length(quadlocsy),length(quadlocsz));
    approx = NaN(length(quadlocsx),length(quadlocsy),length(quadlocsz));
    exact = NaN(length(quadlocsx),length(quadlocsy),length(quadlocsz));
    for iq1 = 1:length(quadlocsx)
    for iq2 = 1:length(quadlocsy)
    for iq3 = 1:length(quadlocsz)
        quadpoint = [quadlocsx(iq1) quadlocsy(iq2) quadlocsz(iq3)];
        weight(iq1,iq2,iq3) = weightsx(iq1)*weightsy(iq2)*weightsz(iq3);
        approx(iq1,iq2,iq3) = vectphi_err(:,:,iq1,iq2,iq3)*DGsoln(:,iv1,iv2,iv3);
        exact(iq1,iq2,iq3) = solution(quadpoint);
    end
    end
    end
    error = abs(approx-exact);
    exactnorm = abs(exact);
    
    L1err_norm = sum(sum(sum(error.*weight)));
    L2err_norm = sum(sum(sum(error.^2.*weight)));
    Lierr_norm = max(max(max(error)));
    L1exact_norm = sum(sum(sum(exactnorm.*weight)));
    L2exact_norm = sum(sum(sum(exactnorm.^2.*weight)));
    Liexact_norm = max(max(max(exactnorm)));

    L1err_norm_cell(iv1,iv2,iv3) = L1err_norm;
    L2err_norm_cell(iv1,iv2,iv3) = L2err_norm;
    Lierr_norm_cell(iv1,iv2,iv3) = Lierr_norm;
    L1exact_norm_cell(iv1,iv2,iv3) = L1exact_norm;
    L2exact_norm_cell(iv1,iv2,iv3) = L2exact_norm;
    Liexact_norm_cell(iv1,iv2,iv3) = Liexact_norm;
end
end
end


L1err_norm_total = sum(sum(sum(L1err_norm_cell)));
L2err_norm_total = sqrt(sum(sum(sum(L2err_norm_cell))));
Lierr_norm_total = max(max(max(Lierr_norm_cell)));
L1exact_norm_total = sum(sum(sum(L1exact_norm_cell)));
L2exact_norm_total = sqrt(sum(sum(sum(L2exact_norm_cell))));
Liexact_norm_total = max(max(max(Liexact_norm_cell)));

Rel_L1err = L1err_norm_total/L1exact_norm_total;
Rel_L2err = L2err_norm_total/L2exact_norm_total;
Rel_Lierr = Lierr_norm_total/Liexact_norm_total;    

data.Rel_L1err = Rel_L1err;
data.Rel_L2err = Rel_L2err;
data.Rel_Lierr = Rel_Lierr;

data.Rel_L1err_filtered = NaN;
data.Rel_L2err_filtered = NaN;
data.Rel_Lierr_filtered = NaN;

%{
%to watch while testing
Rel_L1err = Rel_L1err
Rel_L2err = Rel_L2err
Rel_Lierr = Rel_Lierr
Rel_L1err_filtered = Rel_L1err_filtered
Rel_L2err_filtered = Rel_L2err_filtered
Rel_Lierr_filtered = Rel_Lierr_filtered
%}
