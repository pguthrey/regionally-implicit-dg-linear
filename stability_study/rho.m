function [thiseig] = rho(ath,bth,data)


a = cos(ath)+1i*sin(ath);
b = cos(bth)+1i*sin(bth);
CPmat = 0;
for ix = 1:(2+data.r_param)
    for iy = 1:(2+data.r_param)
        CPmat = CPmat - data.update(:,:,ix,iy)*a^(ix-1)*b^(iy-1);
    end
end
IterMat = eye(data.theta)+CPmat;
thiseig = max(abs(eig(IterMat)));


end

