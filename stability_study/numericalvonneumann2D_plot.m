close all
clear all
clc

M = 6
r = 0

numaxvals(2,4) =  1.2;
numaxvals(2,6) =  1.1;
numaxvals(1,4) = .12;
numaxvals(1,6) =  .06;
delta = 1e-2;
numax = numaxvals(1+r,M);

data.M = M;
data.r_param = r;
savename = ['numericalVN\R' num2str(data.r_param) 'M' num2str(data.M)];
load(savename,'uvals','vvals','rhovals')
        
font = 16;
set(gcf,'Color','white')
uplot = [uvals vvals(:,end:-1:1)];
vplot = [vvals uvals(:,end:-1:1)];
surf(uplot,vplot,rhovals)
if r == 1
    hold on
    switch M
        case 4
            nuli = .12
            plot3([0 nuli nuli],[nuli nuli 0],[1+delta 1+delta 1+delta],'-r','LineWidth',3)
        case 6
            nuli = .06
            plot3([0 nuli nuli],[nuli nuli 0],[1+delta 1+delta 1+delta],'-r','LineWidth',3)            
    end
end
   
%colorbar
view(2)
axis([0 numax 0 numax 0 1+delta 1 1+delta])
shading interp
xlabel('\nu_x','FontSize',font)
ylabel('\nu_y','FontSize',font)
set(gca,'FontSize',font)
set(gca,'linewidth',3)  
colorbar
drawnow         
          
savename = ['numericalVN\final_R' num2str(data.r_param) 'M' num2str(data.M)];
save(savename,'uvals','vvals','rhovals','numax','delta')     

frame = getframe(gcf);
im = frame2im(frame);
[imind,cm] = rgb2ind(im,256);    
print([savename '.png'],'-dpng')        
    
        
        
return

switch r
    case 0                
        title(['LI Approximate stability region , M = ' num2str(M)],'FontSize',font)        
    case 1
        title(['RI1 Approximate stability region , M = ' num2str(M)],'FontSize',font)        
end    
switch r
    case 0                
        print(['numericalVN\results\LI stability M' num2str(M) '.png'],'-dpng')
    case 1
        print(['numericalVN\results\RI1 stability M' num2str(M) '.png'],'-dpng')
end



