function [maxeig] = findrho(R,omega,ares,data)

u = R*cos(omega);
v = R*sin(omega);    
theta = data.theta;
data.nuv1 = 1;
data.nuv2 = 1;
data.appdata.nuf = u;
data.appdata.nug = v;

[update,~,~,~,~,~,~] = initialize_constantcoefficient_update_2D(data);

%%{
data.update = update;
opts = optimoptions('fmincon','Display','off');
rho_obj = @(th) -rho(th(1),th(2),data);
gs = GlobalSearch;
%gs.NumTrialPoints = ares;
problem = createOptimProblem('fmincon','x0',[0;0],...
    'objective',rho_obj,'lb',[0;0],'ub',[2*pi;2*pi],'options',opts);
%[xg,fg,exitflag,output,solutions] = run(gs,problem)
[~,~,~,~,solutions] = run(gs,problem);
maxeig = -solutions(1).Fval;
%} 
%{
maxeig = 0;
[athgrid,bthgrid] = meshgrid(linspace(0,2*pi,ares));
[Nth,Mth] = size(athgrid);   
for ia = 1:Nth
    for ib = 1:Mth
        ath = athgrid(ia,ib);
        bth = bthgrid(ia,ib);
        a = cos(ath)+1i*sin(ath);
        b = cos(bth)+1i*sin(bth);
        CPmat = 0;
        for ix = 1:(2+data.r_param)
            for iy = 1:(2+data.r_param)
                CPmat = CPmat - update(:,:,ix,iy)*a^(ix-1)*b^(iy-1);
            end
        end
        IterMat = eye(theta)+CPmat;
        thiseig = max(abs(eig(IterMat)));
        maxeig = max([thiseig maxeig]);
    end
end
%}

end

