close all
clc


syms tau real
syms xii real
syms eta real
syms zeta real

addpath('predictor')
addpath('corrector')
addpath('compute')
addpath('testfunctions')
addpath('initialize')
addpath('plotting')
addpath('numericalVN')

M = 4
r = 1
data.space_dims = 2;
data.basiscombine = 'full';
data.basis = 'canonical';
data.extra = 'nonlinconvergence1d';
data.M = M;
%data.predictor_solver = 'Fsolve';
data.predictor_solver = 'NewtonIteration';
data.predictorbasis = 'Q';
data.correctorbasis = 'P';

data.r_param = r; 
data.verbose = true;
data.plotIC = false; 
data.plotwhilerunning = false;
data.plotfinal = false;
data.makegif_conserved = false;
data.filter = true;%
data.smartsolver = true;

data.usewaitbars = true;
data.west_symmetry = false;
data.savefile = false;
data.email = true;
data.store = false;
data.methodtype = 'implicit';
data.Neqns = 1;

close all force

[data] = initialize_method(data);
[data] = initialize_Basis(data,data.space_dims);
[data] = initialize_gauss_quadrature(data,data.M);
%[data] = initialize_innerproduct_quadrature(data);
%[data] = initialize_mesh(data);
%[data] = initialize_precompute_testfunctions(data);
%[data] = initialize_formIntegrals(data,data.Tfinal);

matrices.theta = data.theta;
matrices.thetaT = data.thetaT;


M = data.M;
r = data.r_param;

nux = data.nuv1*data.appdata.nuf;
nuy = data.nuv2*data.appdata.nug;
nuz = data.nuv3*data.appdata.nuh;

syms xii real   
test = sym(zeros(1,data.M));
test_diff = sym(zeros(1,data.M));

testeast = zeros(1,data.M);
testwest = zeros(1,data.M);
for i = 1:data.M
    testeast(1,i) = testfunction_Legendre( 1,i );
    testwest(1,i) = testfunction_Legendre( -1,i );    
    test(1,i) = testfunction_Legendre(xii,i );   
    test_diff(1,i) = diff(testfunction_Legendre(xii,i ),xii); 
end
testD = double(int(test_diff'*test,xii,-1,1));


[Xleft,Xright] = meshgrid(data.BO(:,2));
Xint = 2.*(Xleft == Xright);
[Yleft,Yright] = meshgrid(data.BO(:,3));
Yint = 2.*(Yleft == Yright);
[Zleft,Zright] = meshgrid(data.BO(:,4));
Zint = 2.*(Zleft == Zright);
[Tleft,Tright] = meshgrid(data.BO(:,1));
Tint = 2.*(Tleft == Tright);

clear Xleft Xright
clear Yleft Yright
clear Zleft Zright
clear Tleft Tright

Xeast = zeros(1,data.theta);
Xwest = zeros(1,data.theta);
for i = 1:data.thetaT
    Xeast(1,i) = testeast(data.BO(i,2));
    Xwest(1,i) = testwest(data.BO(i,2));
end
Xeasteast_term = Xeast'*Xeast;
Xwesteast_term = Xwest'*Xeast;
Xwestwest_term = Xwest'*Xwest;

Xprime_term = zeros(data.thetaT);
for ileft = 1:data.thetaT
for iright = 1:data.thetaT
   Xprime_term(ileft,iright) = testD(data.BO(ileft,2),data.BO(iright,2));
end
end

Other = Zint.*Yint.*Tint;
X_east = Xeasteast_term.*Other;
X_othr = Xwesteast_term.*Other;
X_west = Xwestwest_term.*Other;
X_der = Xprime_term.*Other;
X_mat = X_east - X_der;

clear Xwest 
clear Xeasteast_term Xwesteast_term Xwestwest_term Xprime_term 
clear X_east X_der

Yeast = zeros(1,data.theta);
Ywest = zeros(1,data.theta);
for i = 1:data.thetaT
    Yeast(1,i) = testeast(data.BO(i,3));
    Ywest(1,i) = testwest(data.BO(i,3));
end
Yeasteast_term = Yeast'*Yeast;
Ywesteast_term = Ywest'*Yeast;
Ywestwest_term = Ywest'*Ywest;

Yprime_term = zeros(data.thetaT);
for ileft = 1:data.thetaT
for iright = 1:data.thetaT
   Yprime_term(ileft,iright) = testD(data.BO(ileft,3),data.BO(iright,3));
end
end

Other = Zint.*Xint.*Tint;
Y_east = Yeasteast_term.*Other;
Y_othr = Ywesteast_term.*Other;
Y_west = Ywestwest_term.*Other;
Y_der = Yprime_term.*Other;
Y_mat = Y_east - Y_der;
clear Ywest 
clear Yeasteast_term Ywesteast_term Ywestwest_term Yprime_term
clear Y_east Y_der

Zeast = zeros(1,data.theta);
Zwest = zeros(1,data.theta);
for i = 1:data.thetaT
    Zeast(1,i) = testeast(data.BO(i,4));
    Zwest(1,i) = testwest(data.BO(i,4));
end
Zeasteast_term = Zeast'*Zeast;
Zwesteast_term = Zwest'*Zeast;
Zwestwest_term = Zwest'*Zwest;

Zprime_term = zeros(data.thetaT);
for ileft = 1:data.thetaT
for iright = 1:data.thetaT
   Zprime_term(ileft,iright) = testD(data.BO(ileft,4),data.BO(iright,4));
end
end

Other = Yint.*Xint.*Tint;
Z_east = Zeasteast_term.*Other;
Z_othr = Zwesteast_term.*Other;
Z_west = Zwestwest_term.*Other;
Z_der = Zprime_term.*Other;
Z_mat = Z_east - Z_der;

clear Zwest 
clear Zeasteast_term Zwesteast_term Zwestwest_term Zprime_term
clear Z_east Z_der

Teast = zeros(1,data.thetaT);
Twest = zeros(1,data.thetaT);
for i = 1:data.thetaT
    Teast(1,i) = testeast(data.BO(i,1));
    Twest(1,i) = testwest(data.BO(i,1));
end
Teasteast_term = Teast'*Teast;
Twesteast_term = Twest'*Teast;

Tprime_term = zeros(data.thetaT);
for ileft = 1:data.thetaT
for iright = 1:data.thetaT
   Tprime_term(ileft,iright) = testD(data.BO(ileft,1),data.BO(iright,1));
end
end

Other = Yint.*Xint.*Zint;
T_east = Teasteast_term.*Other;
T_othr = Twesteast_term.*Other;
T_der = Tprime_term.*Other;
T_mat = T_east - T_der;

clear Teast Twest 
clear Teasteast_term Twesteast_term Tprime_term
clear T_east T_der
clear Xint Yint Zint Tint
disp('matrices crafted')

invLxyz = inv(T_mat + nux*(X_mat-X_west) + nuz*(Z_mat-Z_west) + nuy*(Y_mat-Y_west));
switch data.r_param
    case 1
        %{
        Lxyz = (T_mat + nux*(X_mat-X_west) + nuz*(Z_mat-Z_west) + nuy*(Y_mat-Y_west));
        Lxy = (T_mat + nux*(X_mat-X_west) + nuz*Z_mat + nuy*(Y_mat-Y_west));
        Lyz = (T_mat + nuz*(Z_mat-Z_west) + nux*X_mat + nuy*(Y_mat-Y_west));
        Lxz = (T_mat + nuz*(Z_mat-Z_west) + nuy*Y_mat + nux*(X_mat-X_west));
        Lx = (T_mat + nuz*Z_mat + nuy*Y_mat + nux*(X_mat-X_west));
        Ly = (T_mat + nux*X_mat + nuz*Z_mat + nuy*(Y_mat-Y_west));
        Lz = (T_mat + nux*X_mat + nuy*Y_mat + nuz*(Z_mat-Z_west));
        L = (T_mat + nux*X_mat + nuy*Y_mat + nuz*Z_mat); 
        
        X = nux*X_othr;
        Y = nuy*Y_othr;
        Z = nuz*Z_othr;
        L0 = Lxyz.*0;
        
        %}
        LHS = 1;
        %{
        [[Lxyz L0 L0 L0 L0 L0 L0 L0];
                [-X Lyz L0 L0 L0 L0 L0 L0];
                [-Y L0 Lxz L0 L0 L0 L0 L0];
                [L0 -Y -X Lz L0 L0 L0 L0];
                [-Z L0 L0 L0 Lxy L0 L0 L0];
                [L0 -Z L0 L0 -X Ly L0 L0];
                [L0 L0 -Z L0 -Y L0 Lx L0];
                [L0 L0 L0 -Z L0 -Y -X L];]
                ;
        %}
                     
        invLxy = inv(T_mat + nux*(X_mat-X_west) + nuz*Z_mat + nuy*(Y_mat-Y_west));
        invLyz = inv(T_mat + nuz*(Z_mat-Z_west) + nux*X_mat + nuy*(Y_mat-Y_west));
        invLxz = inv(T_mat + nuz*(Z_mat-Z_west) + nuy*Y_mat + nux*(X_mat-X_west));
        invLx = inv(T_mat + nuz*Z_mat + nuy*Y_mat + nux*(X_mat-X_west));
        invLy = inv(T_mat + nux*X_mat + nuz*Z_mat + nuy*(Y_mat-Y_west));
        invLz = inv(T_mat + nux*X_mat + nuy*Y_mat + nuz*(Z_mat-Z_west));
        invL = inv(T_mat + nux*X_mat + nuy*Y_mat + nuz*Z_mat); 

        clear T_mat
        clear X_mat Y_mat Z_mat
        clear X_west Y_west Z_west

        
        predictor_update = zeros(data.thetaT,data.thetaT,2,2,2);


        
        disp('matrices inverted')
        
        predictor_update(:,:,1,1,2) = nuz*(invL*(Z_othr*(invLz)));
        predictor_update(:,:,1,2,1) = nuy*(invL*(Y_othr*(invLy)));
        predictor_update(:,:,2,1,1) = nux*(invL*(X_othr*(invLx)));    
        M1 = invL*(X_othr*(invLx*Y_othr) + Y_othr*(invLy*X_othr));
        M2 = invL*(Y_othr*(invLy*Z_othr) + Z_othr*(invLz*Y_othr));
        clear invLy 
        M3 = invL*(X_othr*(invLx*Z_othr) + Z_othr*(invLz*X_othr));
        clear invLx
        clear invLz
        predictor_update(:,:,2,2,2) = nux*nuy*nuz*(M1*(invLxy)*Z_othr+M2*(invLyz)*X_othr+M3*(invLxz)*Y_othr)*(invLxyz);
        clear Lxyz
        predictor_update(:,:,2,2,1) = nux*nuy*M1*(invLxy);
        clear M1 invLxy
        predictor_update(:,:,1,2,2) = nuz*nuy*M2*(invLyz);
        clear M2 invLyz
        predictor_update(:,:,2,1,2) = nux*nuz*M3*(invLxz);
        clear M3 invLxz

        clear invLx X_othr

        predictor_update(:,:,1,1,1) = invL;
                
        %clear M1 M2 M3
        clear L 
        clear Lxy 
        %clear Lxyz Lyz Lxy Lz

        
    case 0
        predictor_update = zeros(data.thetaT,data.thetaT,1,1,1);
        predictor_update(:,:,1,1,1) = invLxyz;
        LHS = inv(invLxyz);
        
end

disp('making corrector')


[Tleft,Tright] = meshgrid(data.BO(:,1),1+0.*data.BOs(:,1));
Tint = 2.*(Tleft == Tright);
[Xleft,Xright] = meshgrid(data.BO(:,2),data.BOs(:,1));
Xint = 2.*(Xleft == Xright);
[Yleft,Yright] = meshgrid(data.BO(:,3),data.BOs(:,2));
Yint = 2.*(Yleft == Yright);
[Zleft,Zright] = meshgrid(data.BO(:,4),data.BOs(:,3));
Zint = 2.*(Zleft == Zright);

Xseast = zeros(1,data.theta);
Xswest = zeros(1,data.theta);
for i = 1:data.theta
    Xseast(1,i) = testeast(data.BOs(i,1));
    Xswest(1,i) = testwest(data.BOs(i,1));
end
Xeasteast_term = Xseast'*Xeast;
Xwesteast_term = Xswest'*Xeast;

Xprime_term = zeros(data.theta,data.thetaT);
for ileft = 1:data.theta
for iright = 1:data.thetaT
   Xprime_term(ileft,iright) = testD(data.BOs(ileft,1),data.BO(iright,2));
end
end

Other = Zint.*Yint.*Tint;
X_east = Xeasteast_term.*Other;
X_othr = Xwesteast_term.*Other;
X_der = Xprime_term.*Other;
X_mat = X_east - X_der;

clear Xswest Xseast Xeast
clear Xeasteast_term Xwesteast_term Xwestwest_term Xprime_term 
clear X_east X_der


Yseast = zeros(1,data.theta);
Yswest = zeros(1,data.theta);
for i = 1:data.theta
    Yseast(1,i) = testeast(data.BOs(i,2));
    Yswest(1,i) = testwest(data.BOs(i,2));
end
Yeasteast_term = Yseast'*Yeast;
Ywesteast_term = Yswest'*Yeast;

Yprime_term = zeros(data.theta,data.thetaT);
for ileft = 1:data.theta
for iright = 1:data.thetaT
   Yprime_term(ileft,iright) = testD(data.BOs(ileft,2),data.BO(iright,3));
end
end

Other = Zint.*Xint.*Tint;
Y_east = Yeasteast_term.*Other;
Y_othr = Ywesteast_term.*Other;
Y_der = Yprime_term.*Other;
Y_mat = Y_east - Y_der;

clear Yswest Yseast Yeast
clear Yeasteast_term Ywesteast_term Ywestwest_term Yprime_term 
clear Y_east Y_der

Zseast = zeros(1,data.theta);
Zswest = zeros(1,data.theta);
for i = 1:data.theta
    Zseast(1,i) = testeast(data.BOs(i,3));
    Zswest(1,i) = testwest(data.BOs(i,3));
end
Zeasteast_term = Zseast'*Zeast;
Zwesteast_term = Zswest'*Zeast;

Zprime_term = zeros(data.theta,data.thetaT);
for ileft = 1:data.theta
for iright = 1:data.thetaT
   Zprime_term(ileft,iright) = testD(data.BOs(ileft,3),data.BO(iright,4));
end
end

Other = Yint.*Xint.*Tint;
Z_east = Zeasteast_term.*Other;
Z_othr = Zwesteast_term.*Other;
Z_der = Zprime_term.*Other;
Z_mat = Z_east - Z_der;

clear Zswest Zseast Zeast
clear Zeasteast_term Zwesteast_term Zwestwest_term Zprime_term 
clear Z_east Z_der

corrector_update = zeros(data.theta,data.thetaT,2,2,2);
corrector_update(:,:,1,1,1) = nux*X_mat+nuy*Y_mat+nuz*Z_mat;
corrector_update(:,:,2,1,1) = -nux*X_othr;
corrector_update(:,:,1,2,1) = -nuy*Y_othr;
corrector_update(:,:,1,1,2) = -nuz*Z_othr;

corrector_update = corrector_update/2^(data.space_dims);

[Tprojl,Tprojr] = meshgrid(1+0.*data.BOs(:,1),data.BO(:,1));
[Xprojl,Xprojr] = meshgrid(data.BOs(:,1),data.BO(:,2));
[Yprojl,Yprojr] = meshgrid(data.BOs(:,2),data.BO(:,3));
[Zprojl,Zprojr] = meshgrid(data.BOs(:,3),data.BO(:,4));
Proj = (Tprojl == Tprojr);
Proj = Proj.*(Xprojl == Xprojr);
Proj = Proj.*(Yprojl == Yprojr);
Proj = Proj.*(Zprojl == Zprojr);
clear Tprojl Tprojr
clear Xprojl Xprojr
clear Yprojl Yprojr
clear Zprojl Zprojr


disp('corrector matrices made')


upindexlist = [1 1 1];
upmeshlocslist = [0 0 0];
update = zeros(data.theta,data.theta,r+2,r+2,r+2);

crrlist = [ [0 0 0];
            [-1 0 0];
            [0 -1 0];
            [0 0 -1]];

for ic = 1:length(crrlist(:,1))
    icx = crrlist(ic,1);
    icy = crrlist(ic,2);
    icz = crrlist(ic,3);
    C = corrector_update(:,:,1+abs(icx),1+abs(icy),1+abs(icz));
    for irz = -r:0
    for iry = -r:0
    for irx = -r:0
        P = predictor_update(:,:,1+abs(irx),1+abs(iry),1+abs(irz));        
        iux = 1 + abs(irx) + abs(icx);
        iuy = 1 + abs(iry) + abs(icy);
        iuz = 1 + abs(irz) + abs(icz); 
        mat = C*P*T_othr*Proj;
        if norm(mat) > 0
            upindex = [iux iuy iuz];
            check1 = upindexlist(:,1)==upindex(1);
            check2 = upindexlist(:,2)==upindex(2);
            check3 = upindexlist(:,3)==upindex(3);
            check = sum(check1.*check2.*check3);
            if ~check
                upindexlist = [upindexlist ; upindex];
                upmeshlocs = [icx+irx icy+iry icz+irz];
                upmeshlocslist = [upmeshlocslist ; upmeshlocs ];
            end
            update(:,:,iux,iuy,iuz) = update(:,:,iux,iuy,iuz) + mat;
        end
    end
    end
    end
end
disp('update made')


Xeast = double(int(int(PSI_e_T*PSI_e,eta,-1,1),tau,-1,1));
matrices.Xeast = Xeast;
matrices.Xseast = Xeast(1:data.theta,1:data.theta);
%Xteast = Xeast((data.theta+1):end,(data.theta+1):end);

Xwest=double(int(int(PSI_w_T*PSI_e,eta,-1,1),tau,-1,1));
matrices.Xwest = Xwest;
matrices.Xswest = Xwest(1:data.theta,1:data.theta);
%Xtwest = Xwest((data.theta+1):end,(data.theta+1):end);

Xtrunc = double(int(int(PSI_w_T*PSI_w,eta,-1,1),tau,-1,1));
matrices.Xtrunc = Xtrunc;
Xstrunc = Xtrunc(1:data.theta,1:data.theta);
%Xttrunc = Xtrunc((data.theta+1):end,(data.theta+1):end);

Xprime = double(int(int(int(PSI_dx_T*PSI,eta,-1,1),tau,-1,1),xii,-1,1));
matrices.Xprime = Xprime;
Xsprime = Xprime(1:data.theta,1:data.theta);
matrices.Xscell = matrices.Xseast - Xsprime;
%Xtprime = Xprime((data.theta+1):end,(data.theta+1):end);

Yeast=double(int(int(PSI_n_T*PSI_n,xii,-1,1),tau,-1,1));
matrices.Yeast = Yeast;
matrices.Yseast = Yeast(1:data.theta,1:data.theta);
%Yteast = Yeast((data.theta+1):end,(data.theta+1):end);

Ywest=double(int(int(PSI_s_T*PSI_n,xii,-1,1),tau,-1,1));
matrices.Ywest = Ywest;
matrices.Yswest = Ywest(1:data.theta,1:data.theta);
%Ytwest = Ywest((data.theta+1):end,(data.theta+1):end);

Ytrunc =double(int(int(PSI_s_T*PSI_s,xii,-1,1),tau,-1,1));
matrices.Ytrunc = Ytrunc;
%Ystrunc = Ytrunc(1:data.theta,1:data.theta);
%Yttrunc = Ytrunc((data.theta+1):end,(data.theta+1):end);

Yprime = double(int(int(int(PSI_dy_T*PSI,xii,-1,1),tau,-1,1),eta,-1,1));
matrices.Yprime = Yprime;
Ysprime = Yprime(1:data.theta,1:data.theta);
matrices.Yscell = matrices.Yseast - Ysprime;
%Ytprime = Yprime((data.theta+1):end,(data.theta+1):end);

Teast = double(int(int(PSI_f_T*PSI_f,eta,-1,1),xii,-1,1));
matrices.Teast = Teast;
%Tseast = Teast(1:data.theta,1:data.theta);
%matrices.Tceast = Teast(1:data.theta,(data.theta+1):end);
%TcTeast = Teast((data.theta+1):end,1:data.theta);
%Tteast = Teast((data.theta+1):end,(data.theta+1):end);

Twest = double(int(int(PSI_p_T*PSI_f,eta,-1,1),xii,-1,1));
matrices.Twest = Twest;
%Tswest = Twest(1:data.theta,1:data.theta);
%Tcwest = Twest(1:data.theta,(data.theta+1):end);
%TcTwest = Twest((data.theta+1):end,1:data.theta);
%Ttwest = Twest((data.theta+1):end,(data.theta+1):end);

Tprime = double(int(int(int(PSI_dt_T*PSI,eta,-1,1),tau,-1,1),xii,-1,1));
matrices.Tprime = Tprime;
%Tsprime = Tprime(1:data.theta,1:data.theta);
%Tcprime = Tprime(1:data.theta,(data.theta+1):end);
%TcTprime = Tprime((data.theta+1):end,1:data.theta);
%Ttprime = Tprime((data.theta+1):end,(data.theta+1):end);

matrices.D = data.space_dims;
matrices.data.theta = data.theta;
projproduct = int(int(int(PSI'*PHI,tau,-1,1),xii,-1,1),eta,-1,1);
matrices.Proj = projproduct/(2^(data.space_dims+1));


    windvals = linspace(0,pi/2,50);

    figure 
    axis([0 1.4 0 1.4])
    deltar = .05;
    hold on
    Rmax = 1;
    Rmin = .5;
    errtol = 1e-12;
    deltatol = 1e-12;

    switch r 
        case 1
        findrho = @(Rmax,omega) findrhoRI1(Rmax,omega,matrices);
        case 0
        findrho = @(Rmax,omega) findrhoLI(Rmax,omega,matrices);
    end                
    target = 1+1e-11;
    close all
    clear maxline
    clear minline
    for k = 1:length(windvals)    
        maxeig = 0;
        omega = windvals(k);
        west = 1;
        [maxeig] = findrho(Rmax,omega);
        [mineig] = findrho(Rmin,omega);
        while (target-maxeig)*(target-mineig) > 0
            Rmax = Rmax*1.1;
            Rmin = Rmin*.8;
            [maxeig] = findrho(Rmax,omega);
            [mineig] = findrho(Rmin,omega);
        end
        R = (Rmax+Rmin)/2;
        [maxeig] = findrho(R,omega); 
        while west     
            if (maxeig-target) > 0
                Rmax = R;
            else 
                Rmin = R;
            end
            R = (Rmax+Rmin)/2;        
            [maxeig] = findrho(R,omega); 
            if abs(maxeig-target) < errtol
                if ((Rmax-Rmin)/2) < deltatol
                    west = 0;
                end
            end

        end  
        maxline(k,1) = Rmax*cos(omega);
        maxline(k,2) = Rmax*sin(omega);
        minline(k,1) = Rmin*cos(omega);
        minline(k,2) = Rmin*sin(omega);

        hold off
        cflxline = (maxline(:,1)+minline(:,1))/2;
        cflyline = (maxline(:,2)+minline(:,2))/2;
        [cflmax,C] = min(max(maxline,[],2));
        plot([0 maxline(C,1)],[0 maxline(C,2)],'-k','LineWidth',3)
        hold on
        plot(cflxline,cflyline,'-b','LineWidth',3)
        set(gcf,'Color','white')
        plot([0 0],[0 0],'-k')
        legend('Stability Region',['Max CFL 2D = ' num2str(cflmax)],['Max CFL 1D = ' num2str(maxline(1,1))],'Location','Best')
        font = 16;
        axis([0 1.2 0 1.2])
        switch r
            case 0                
                title(['LI Approximate stability region , M = ' num2str(M)],'FontSize',font)        
            case 1
                title(['RI1 Approximate stability region , M = ' num2str(M)],'FontSize',font)        
        end
        xlabel(['\nu_x'],'FontSize',font)
        ylabel(['\nu_y'],'FontSize',font)
        set(gca,'FontSize',font)%,'XTick',-1:.25:1,'YTick',-1:.25:1)
        set(gca,'linewidth',3)
        drawnow
    end
    
    switch r
        case 0                
            print(['results\LI stability M' num2str(M) '.png'],'-dpng')
        case 1
            print(['results\RI1 stability M' num2str(M) '.png'],'-dpng')
    end



