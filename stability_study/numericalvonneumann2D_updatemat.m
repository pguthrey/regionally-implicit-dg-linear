close all
clear all
clc

addpath('predictor')
addpath('corrector')
addpath('compute')
addpath('testfunctions')
addpath('initialize')
addpath('plotting')
addpath('numericalVN')

estimate_boundary = false;
estimate_region = true;

delta = 1e-2;

%r  = 1
u1vals(2,4) = .8;
u2vals(2,4) = 1.2;
numaxvals(2,4) =  1.2;
u1vals(2,6) = .8;
u2vals(2,6) = 1.1;
numaxvals(2,6) =  1.1;

%r  = 0
u1vals(1,4) = .075;
u2vals(1,4) = .12;
numaxvals(1,4) = .12;
u1vals(1,6) = .043;
u2vals(1,6) = .051;
numaxvals(1,6) =  .06;

for M = [4 6]
for r = [0 1]

    data.space_dims = 2;
    data.basiscombine = 'full';
    data.basis = 'canonical';
    data.extra = 'nonlinconvergence1d';
    data.M = M;
    %data.predictor_solver = 'Fsolve';
    data.predictor_solver = 'NewtonIteration';
    data.predictorbasis = 'Q';
    data.correctorbasis = 'P';

    data.r_param = r; 
    data.verbose = true;
    data.plotIC = false; 
    data.plotwhilerunning = false;
    data.plotfinal = false;
    data.makegif_conserved = false;
    data.filter = true;%
    data.smartsolver = true;

    data.usewaitbars = true;
    data.west_symmetry = false;
    data.savefile = false;
    data.email = true;
    data.store = false;
    data.methodtype = 'implicit';
    data.Neqns = 1;
    
    [data] = initialize_method(data);
    [data] = initialize_Basis(data,data.space_dims);
    [data] = initialize_gauss_quadrature(data,data.M);
    %[data] = initialize_innerproduct_quadrature(data);
    %[data] = initialize_mesh(data);
    %[data] = initialize_precompute_testfunctions(data);
    %[data] = initialize_formIntegrals(data,data.Tfinal);

%{
    
 ares = 500;
    for i = 1:26
    rhoj(i) = findrho(i/20,pi/4,ares,data)-1
end
plot((1:26)/20,rhoj(1:26))
    
keyboard 
%}
    
    
        numax = numaxvals(1+r,M);
        Nres = 100;
        ares = 100;    
        
        figure(1)         
        
        cfl = data.cfl;

        %switch data.r_param
        %    case 0
                data.space_dims = 1;
                [data] = initialize_method(data);
                cfl = data.cfl;
                data.space_dims = 2;
                u1 = u1vals(1+r,M);%cfl;
                u2 =  u2vals(1+r,M);%cfl+.1;
                cflvals = [linspace(0,.95*u1,2) linspace(.95*u1,u2,Nres-2)];
                %cflvals = [linspace(u1,u2,Nres)];
                [thetavals,Rgrid] = meshgrid(linspace(0,pi/4,Nres),cflvals);
                %Rvals = Rgrid./(cos(thetavals)+sin(thetavals));                
                Rvals = Rgrid./cos(thetavals);                
                %uvals = R.*cos(theta1);
                %vvals = R.*sin(theta1);                
        %end
                
        %[uvals,vvals] = meshgrid(linspace(0,numax,Nres));
        
        rhovals = zeros([1 2].*size(Rvals));
        font = 16;
        set(gcf,'Color','white')
        uvals = Rvals.*cos(thetavals);
        vvals = Rvals.*sin(thetavals);

        for ivx = 1:(Nres)%:-1:1
        for ivy = 1:(Nres)%:-1:1
            
            if rhovals(ivx,ivy) == 0        
                
               R = Rvals(ivx,ivy);
               omega = thetavals(ivx,ivy);
               val1 = findrho(R,omega,ares,data);
               val2 = findrho(R,omega,ares,data);
               rhovals(ivx,ivy) = max(val1,val2);
               %{
               bad = rhovals(ivx,ivy);
               if bad > 1+delta
                   rhovals(ivx:Nres,ivy) = bad;
                   rhovals(ivx:Nres,end+1-ivy) = bad;
                   %keyboard 
               end
                %}
               rhovals(ivx,end+1-ivy) = rhovals(ivx,ivy);
               
               %%{
                figure(1)
                uplot = [uvals vvals(:,end:-1:1)];
                vplot = [vvals uvals(:,end:-1:1)];
                surf(uplot,vplot,rhovals)
                %colorbar
                view(2)
                axis([0 numax 0 numax 0 1+delta 1 1+delta])
                
               
                shading interp
                xlabel('\nu_x','FontSize',font)
                ylabel('\nu_y','FontSize',font)
                set(gca,'FontSize',font)
                set(gca,'linewidth',3)  
                colorbar
                drawnow
                %}
                %{
                figure(2)
                surf(rhovals-1)
                   shading interp
                axis([1 Nres 1 Nres*2 -1 delta 0 delta])
                 view(2)
                   xlabel('\nu_x','FontSize',font)
                ylabel('\nu_y','FontSize',font)
                set(gca,'FontSize',font)
                set(gca,'linewidth',3)  
                colorbar
                drawnow
                %}
            end
            
        end
        end
        uplot = [uvals vvals(:,end:-1:1)];
        vplot = [vvals uvals(:,end:-1:1)];
        surf(uplot,vplot,rhovals)
        %colorbar
        view(2)
        axis([0 numax 0 numax 0 1+delta 1 1+delta])
        shading interp
        xlabel('\nu_x','FontSize',font)
        ylabel('\nu_y','FontSize',font)
        set(gca,'FontSize',font)
        set(gca,'linewidth',3)  
        colorbar
        drawnow         
        
        savename = ['numericalVN\R' num2str(data.r_param) 'M' num2str(data.M)];
        save(savename,'uvals','vvals','rhovals','numax','delta')     

        frame = getframe(gcf);
        im = frame2im(frame);
        [imind,cm] = rgb2ind(im,256);    
        print([savename '.png'],'-dpng')        
    
end
end


%{
frame = getframe(gcf);
im = frame2im(frame);
[imind,cm] = rgb2ind(im,256);    
print([savename '.png'],'-dpng')
%}
rhonanvals = nthroot(rhovals-1,3)+1;
rhonanvals(rhonanvals>1) = NaN;

