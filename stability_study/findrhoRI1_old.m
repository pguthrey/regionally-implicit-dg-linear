function [maxeig] = findrhoRI1(R,omega,matrices)

u = R*cos(omega);
v = R*sin(omega);    


D = matrices.D;
theta = matrices.theta;

Xwest = matrices.Xwest;
Xeast = matrices.Xeast;
Xprime = matrices.Xprime;
Xtrunc = matrices.Xtrunc;

Ywest = matrices.Ywest;
Yeast = matrices.Yeast;
Yprime = matrices.Yprime;
Ytrunc = matrices.Ytrunc;

Twest = matrices.Twest;
Teast = matrices.Teast;
Tprime = matrices.Tprime;

Lxy = Teast - Tprime + u*(Xeast - Xprime - Xtrunc) + v*(Yeast - Yprime - Ytrunc);
Ly = Teast - Tprime + u*(Xeast - Xprime) + v*(Yeast - Yprime - Ytrunc);
Lx = Teast - Tprime + u*(Xeast - Xprime - Xtrunc) + v*(Yeast - Yprime);
L0 = Teast - Tprime + u*(Xeast - Xprime) + v*(Yeast - Yprime);

Mat1 = Lxy\Twest;%
Mat2 = Ly\Twest;
Mat3 = Ly\matrices.Xwest*Mat1;
Mat4 = Lx\Twest;%
Mat5 = Lx\matrices.Ywest*Mat1; %
Mat6 = (L0\Twest)*matrices.Proj;
Mat7 = (L0\(Xwest*Mat4))*matrices.Proj;
Mat8 = (L0\(Xwest*Mat5))*matrices.Proj;
Mat9 = (L0\(Ywest*Mat2))*matrices.Proj;
Mat10 = (L0\(Ywest*Mat3))*matrices.Proj;
Mat11 = Mat8 + Mat10;

Xscell = matrices.Xscell;
Xswest = matrices.Xswest;
Yscell = matrices.Yscell;
Yswest = matrices.Yswest;
%%{
M0 = (u*Xscell+v*Yscell)*Mat6;
Ma = -u*Xswest*Mat6+u^2*Xscell*Mat7+u*v*Yscell*Mat7;
Mb = -v*Yswest*Mat6+v^2*Yscell*Mat9+u*v*Xscell*Mat9;
Mb2 = -v^2*Yswest*Mat9;
Mab = -u*v*(Yswest*Mat7+Xswest*Mat9)+(u^2*v*Xscell+u*v^2*Yscell)*Mat11;
Ma2 = -u^2*Xswest*Mat7;
Mab2 = -u*v^2*Yswest*Mat11;
Ma2b = -u^2*v*Xswest*Mat11;
%}
maxeig = 0;

[athgrid,bthgrid] = meshgrid(linspace(0,2*pi,50));
[Nth,Mth] = size(athgrid);   

for i = 1:Nth
    for j = 1:Mth
        ath = athgrid(i,j);
        bth = bthgrid(i,j);
        a = cos(ath)+1i*sin(ath);
        b = cos(bth)+1i*sin(bth);
        CPmat = M0+a*Ma+b*Mb+b^2*Mb2+a*b*Mab+a^2*Ma2+a*b^2*Mab2+a^2*b*Ma2b; 
        %Corrx = u*(matrices.Xscell - a*);
        %Corry = v*(matrices.Yscell - b*matrices.Yswest);
        %Corr = Corrx + Corry;
        %Pmat = Mat6+a*u*Mat7+v*u*a*b*Mat11+b*v*Mat9;
        IterMat = eye(theta)-CPmat/2^(D+1);
        thiseig = max(abs(eig(IterMat)));
        maxeig = max([thiseig maxeig]);
    end
end



end

