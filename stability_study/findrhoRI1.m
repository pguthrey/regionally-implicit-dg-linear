function [maxeig] = findrhoRI1(R,omega,ares,data)

u = R*cos(omega);
v = R*sin(omega);    
theta = data.theta;
data.nuv1 = 1;
data.nuv2 = 1;
data.appdata.nuf = u;
data.appdata.nug = v;

[update,~,~,~,~,~,~] = initialize_constantcoefficient_update_2D(data);


maxeig = 0;

[athgrid,bthgrid] = meshgrid(linspace(0,2*pi,ares));
[Nth,Mth] = size(athgrid);   

for ia = 1:Nth
    for ib = 1:Mth
        ath = athgrid(ia,ib);
        bth = bthgrid(ia,ib);
        a = cos(ath)+1i*sin(ath);
        b = cos(bth)+1i*sin(bth);
        CPmat = 0;
        for ix = 1:3
            for iy = 1:3
                CPmat = CPmat - update(:,:,ix,iy)*a^(ix-1)*b^(iy-1);
            end
        end
        IterMat = eye(theta)+CPmat;
        thiseig = max(abs(eig(IterMat)));
        maxeig = max([thiseig maxeig]);
    end
end



end

