close all
clear all
clc

addpath('predictor')
addpath('corrector')
addpath('compute')
addpath('testfunctions')
addpath('initialize')
addpath('plotting')
addpath('numericalVN')

estimate_boundary = false;
estimate_region = true;

delta = 1e-2;

%r  = 1
u1vals(2,4) = .8;
u2vals(2,4) = 1.2;
numaxvals(2,4) =  1.2;
u1vals(2,6) = .8;
u2vals(2,6) = 1.1;
numaxvals(2,6) =  1.1;

%r  = 0
u1vals(1,4) = .075;
u2vals(1,4) = .12;
numaxvals(1,4) = .12;
u1vals(1,6) = .043;
u2vals(1,6) = .051;
numaxvals(1,6) =  .06;

M = 2
r = 0

data.space_dims = 1;
data.basiscombine = 'full';
data.basis = 'canonical';
data.extra = 'nonlinconvergence1d';
data.M = M;
%data.predictor_solver = 'Fsolve';
data.predictor_solver = 'NewtonIteration';
data.predictorbasis = 'Q';
data.correctorbasis = 'P';

data.r_param = r; 
data.verbose = true;
data.plotIC = false; 
data.plotwhilerunning = false;
data.plotfinal = false;
data.makegif_conserved = false;
data.filter = true;%
data.smartsolver = true;

data.usewaitbars = true;
data.west_symmetry = false;
data.savefile = false;
data.email = true;
data.store = false;
data.methodtype = 'implicit';
data.Neqns = 1;

[data] = initialize_method(data);
[data] = initialize_Basis(data,data.space_dims);
[data] = initialize_gauss_quadrature(data,data.M);
%[data] = initialize_innerproduct_quadrature(data);
%[data] = initialize_mesh(data);
%[data] = initialize_precompute_testfunctions(data);
%[data] = initialize_formIntegrals(data,data.Tfinal);
syms nu
data.cfl = nu;

data.nuv1 = nu;
data.appdata.nuf = 1;

[update,~,~,~,~,~] = initialize_constantcoefficient_update_1D(data);

syms ath

a = cos(ath)+1i*sin(ath);
CPmat = 0;
for ix = 1:(2+data.r_param)
    CPmat = CPmat - update(:,:,ix)*a^(ix-1);
end
syms lambda
IterMat = (1-lambda)*eye(data.theta)+CPmat;
poly = collect(det(IterMat),lambda)
C = subs(poly,lambda,0)
B = subs( simplify((poly-C)/lambda) ,lambda,0)
A = 1

lambda1 = (-B+sqrt(B^2-4*C))/2
lambda2 = (-B-sqrt(B^2-4*C))/2
expand(B*conj(B))

abs(double(subs([lambda1 lambda2],[nu ath],[1/3 0])))

abs(subs([lambda1 ; lambda2],[nu],[1/3]))










