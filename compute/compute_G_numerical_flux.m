function [ Flux ,speed ] = compute_G_numerical_flux(ql,qr,quadpoint,data)
% Evaluates the data.numerical flux for the given problem
% written by Pierson Guthrey
% -------------------------------------------------
% INPUTS    ql
%           qr
% OUTPUTS   Flux
% Note: other variables may be %loaded in from the problem parameter files
% ------------------------------------------------------------------------

%Rusanov Flux
qa = (ql+qr)/2;
Gl = problem_G(ql,quadpoint,data);
Gr = problem_G(qr,quadpoint,data);

[speed] = compute_G_wavespeed(ql,qr,quadpoint,data);

Flux = (Gl+Gr-speed.*(qr-ql))./2;

end

