function [ Jacobian ] = compute_F_dflux_dql(ql,qr,quadpoint,data)
% Evaluates the flux Jacobian for the given problem
% written by Pierson Guthrey
% -------------------------------------------------
% INPUTS    q
% OUTPUTS   Jacobian
% Note: other variables may be %loaded in from the problem parameter files
% ------------------------------------------------------------------------

%Rusanov Flux
dFdql = problem_F_FluxJacobian(ql,quadpoint,data);
[speed] = compute_F_wavespeed(ql,qr,quadpoint,data);
[dspeeddql] = compute_F_dwavespeed_dql(ql,qr,quadpoint,data);

Jacobian = (dFdql+speed.*(eye(length(ql))) - (qr-ql)*dspeeddql)./2;

end

