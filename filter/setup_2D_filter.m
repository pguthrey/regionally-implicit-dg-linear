clc
clear all
addpath('filter')
syms xtilde real
syms ytilde real
syms xii real
syms eta real
syms qx qy
syms Qx Qy

%Sets up the filter by evaluating several integrals 
%This is very 'under the hood'

addpath('testfunctions')
addpath('initialize')
addpath('filter')
DGxcells = 0;
DGycells = 0;
quadlocsfilter = 0;
for order = 1

    Cx = sym('Cx',[1,order]);
    Cy = sym('Cy',[1,order]);

    %Find spline coefficients
    k = order - 1;
    % convolution -- same kernel for both directions
    [bscoeffs,bsbreaks] = choose_BSpline_coeffs( order );
    coeff = calcKernelCoeffL( order, 0 );
    pieces = length(bsbreaks)-1;
    coeff_num = floor(1:.5:(pieces+.5));
    [coeff_x,coeff_y] = meshgrid(coeff_num);
    [type_x,type_y] = meshgrid(mod((1:(2*pieces))-1,2)+1);
    kernels = length(coeff);

    %Set up DG basis
    data.M = order;
    data.space_dims = 2;
    data.r_param = 0;
    data.Neqns = 1;
    data.basis = 'canonical' ;
    data.predictorbasis = 'Q';
    data.correctorbasis = 'P';
    [data] = initialize_Basis(data,data.space_dims);    
    
    for kay = 1:data.theta
        for eqn = 1:data.Neqns
        ells = data.sysBOs(eqn,kay);
        coeffs = data.sysBOscoeffs(eqn,kay);   
        quadpoint = [xii,eta,0];
        %Phi terms
        vectphi(eqn,kay) =  testfunction_phi(quadpoint,ells,data).*coeffs;
        end
    end
    %Set up arbitrary filter coefficients
    px = Cx*xtilde.^[(order-1):-1:0]';
    py = Cy*ytilde.^[(order-1):-1:0]';

    xtilde1 = (1-Qx)/2*qx;
    xtilde2 = (1+Qx)/2*qx+(1-Qx)/2;
    ytilde1 = (1-Qy)/2*qy;
    ytilde2 = (1+Qy)/2*qy+(1-Qy)/2;

    clear filter 
    %Bounds Type 1 in x Type 1 in y
    integrand1 = subs(px*py*vectphi,[xii eta xtilde ytilde], ...
                [(1-Qx)*qx+Qx (1-Qy)*qy+Qy xtilde1 ytilde1]);
    filter(:,:,1,1) = (1-Qx)/2*(1-Qy)/2*int(int(integrand1,qx,0,1),qy,0,1);
    %Bounds Type 2 in x Type 1 in y
    integrand2 = subs(px*py*vectphi,[xii eta xtilde ytilde], ...
                [(1+Qx)*qx-1 (1-Qy)*qy+Qy xtilde2 ytilde1]);
    filter(:,:,2,1) = (1+Qx)/2*(1-Qy)/2*int(int(integrand2,qx,0,1),qy,0,1);
    %Bounds Type 1 in x Type 2 in y
    integrand3 = subs(px*py*vectphi,[xii eta xtilde ytilde], ...
                [(1-Qx)*qx+Qx (1+Qy)*qy-1 xtilde1 ytilde2]);
    filter(:,:,1,2) = (1-Qx)/2*(1+Qy)/2*int(int(integrand3,qx,0,1),qy,0,1);
    %Bounds Type 2 in x Type 2 in y
    integrand4 = subs(px*py*vectphi,[xii eta xtilde ytilde], ...
                [(1+Qx)*qx-1 (1+Qy)*qy-1 xtilde2 ytilde2]);
    filter(:,:,2,2) = (1+Qx)/2*(1+Qy)/2*int(int(integrand4,qx,0,1),qy,0,1);

    clear bspline
    for ix = 1:(2*pieces)
    for iy = 1:(2*pieces)
        mat = filter(:,:,type_x(ix,iy),type_y(ix,iy));
        matsubbed = subs(mat,[Cx Cy], ...
            [bscoeffs(coeff_x(ix,iy),:) bscoeffs(coeff_y(ix,iy),:)]);
        bspline(:,:,ix,iy) = matsubbed;
    end
    end

    fullfilter = sym(zeros(data.Neqns,data.theta,2*(kernels-1)+2*pieces,2*(kernels-1)+2*pieces));
    
    %%{
    for cx = 1:kernels
    for cy = 1:kernels
        icx = 2*(cx-1);
        icy = 2*(cy-1);
        for ix = 1:(2*pieces)
        for iy = 1:(2*pieces)
        fullfilter(:,:,icx+ix,icy+iy) = fullfilter(:,:,icx+ix,icy+iy) + bspline(:,:,ix,iy)*coeff(cx)*coeff(cy);
        end
        end
    end
    donepart1 = cx/kernels
    end
    maxdeltacell = pieces/2 + (length(coeff)-1)/2;
    DGcells = floor([-maxdeltacell -(maxdeltacell-1):.5:(maxdeltacell-.5) maxdeltacell]);
    [DGxcells,DGycells] = meshgrid(DGcells);
    clear quadlocsfilter
    for iqx = 1:length(quadlocs) 
    for iqy = 1:length(quadlocs) 
        mat = subs(fullfilter,[Qx Qy],[quadlocs(iqx) quadlocs(iqy)]);
        quadlocsfilter(:,:,:,:,iqx,iqy) = double(mat);

    end
    donepart2 = iqx/length(quadlocs)
    end
    
    data = initialize_gauss_lobatto_quadrature(data,order+3);
    quadlocs = data.locs;
    quadwgts = data.wgts1D;
    filtername = ['filterdata/filterdata_' num2str(data.space_dims) 'D_M' num2str(data.M) '_' data.correctorbasis 'basis'];
    save(filtername,'quadlocsfilter','DGxcells','DGycells','quadlocs','quadwgts')
end
