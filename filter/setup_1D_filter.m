clc
clear all
addpath('filter')
syms xtilde real
syms xii real
syms cx0 cx1 cx2 cx3
syms qx
syms Qx

%Sets up the filter by evaluating several integrals 
%This is very 'under the hood'


for M = 2
    order = M;
    clear filter bspline fullfilter quadlocsfilter
    Cx = sym('Cx',[1,order]);

    %Find spline coefficients
    k = order - 1;
    % convolution -- same kernel for both directions
    [bscoeffs,bsbreaks] = choose_BSpline_coeffs( order );
    %bs = getBSplinePP( order );
    coeff = calcKernelCoeffL( order, 0 );
    pieces = length(bsbreaks)-1;
    coeff_x = floor(1:.5:(pieces+.5));
    type_x = mod((1:(2*pieces))-1,2)+1;
    kernels = length(coeff);

    %Set up DG basis
    data.M = order;
    data.space_dims = 1;
    data.r_param = 0;
    data.Neqns = 1;
    data.basis = 'canonical' ;
    data.predictorbasis = 'Q';
    data.correctorbasis = 'P';
    [data] = initialize_Basis(data,data.space_dims);
    for kay = 1:data.theta
        for eqn = 1:data.Neqns
        ells = data.sysBOs(eqn,kay);
        coeffs = data.sysBOscoeffs(eqn,kay);   
        quadpoint = [xii,0,0];
        %Phi terms
        vectphi(eqn,kay) =  testfunction_phi(quadpoint,ells,data).*coeffs;
        end
    end

    %Set up arbitrary filter coefficients
    px = Cx*xtilde.^[(order-1):-1:0]';


    xtilde1 = (1-Qx)/2*qx;
    xtilde2 = (1+Qx)/2*qx+(1-Qx)/2;

    %Bounds Type 1 in x Type 1 in y
    integrand1 = subs(px*vectphi,[xii xtilde],[(1-Qx)*qx+Qx xtilde1]);
    filter(:,:,1) = (1+Qx)/2*int(integrand1,qx,0,1);
    %Bounds Type 2 in x Type 1 in y
    integrand2 = subs(px*vectphi,[xii xtilde],[(1+Qx)*qx-1 xtilde2]);
    filter(:,:,2) = (1-Qx)/2*int(integrand2,qx,0,1);

    for ix = 1:(2*pieces)
        mat = filter(:,:,type_x(ix));
        matsubbed = subs(mat,Cx,bscoeffs(coeff_x(ix),:));
        bspline(:,:,ix) = matsubbed;
    end

    fullfilter = sym(zeros(data.Neqns,data.theta,2*(kernels-1)+2*pieces));
    for cx = 1:kernels
        icx = 2*(cx-1);
        for ix = 1:(2*pieces)
        fullfilter(:,:,icx+ix) = fullfilter(:,:,icx+ix) + bspline(:,:,ix)*coeff(cx);
        end
    end

    maxdeltacell = pieces/2 + (length(coeff)-1)/2;
    DGcells = floor([-maxdeltacell -(maxdeltacell-1):.5:(maxdeltacell-.5) maxdeltacell]);
    DGxcells = DGcells;

    data = initialize_gauss_lobatto_quadrature(data,order+3);

    quadlocs = data.locs;
    quadwgts = data.wgts1D;


    for iqx = 1:length(quadlocs)  
        mat = subs(fullfilter,Qx,quadlocs(iqx));
        quadlocsfilter(:,:,:,iqx) = double(mat);
    end

    filtername = ['filterdata/filterdata_' num2str(data.space_dims) 'D_M' num2str(data.M) '_' data.correctorbasis 'basis'];
    save(filtername,'quadlocsfilter','DGxcells','quadlocs','quadwgts')

end