clc
clear all
addpath('filter')
syms xtilde real
syms ytilde real
syms ztilde real
syms xii real
syms eta real
syms zeta real
syms qx qy qz

%Sets up the filter by evaluating several integrals 
%This is very 'under the hood'

addpath('testfunctions')
addpath('initialize')
addpath('filter')
DGxcells = 0;
DGycells = 0;
DGzcells = 0;
quadlocsfilter = 0;
order = 8;
data.M = order;
clear quadlocsfilter
data = initialize_gauss_lobatto_quadrature(data,order);
quadlocs = data.locs;
quadwgts = data.wgts1D;
          
h1 = waitbar(0,'Creating Filter')
    
%Find spline coefficients
    k = order - 1;
    % convolution -- same kernel for both directions
    [bscoeffs,bsbreaks] = choose_BSpline_coeffs( order );
    coeff = calcKernelCoeffL( order, 0 );
    pieces = length(bsbreaks)-1;
    coeff_num = floor(1:.5:(pieces+.5));
    for ix = 1:length(coeff_num)
    for iy = 1:length(coeff_num)
    for iz = 1:length(coeff_num)
        coeff_x(ix,iy,iz) = coeff_num(ix);
        coeff_y(ix,iy,iz) = coeff_num(iy);
        coeff_z(ix,iy,iz) = coeff_num(iz);
    end
    end
    end
    pieces_shift = mod((1:(2*pieces))-1,2)+1;
    for ix = 1:length(pieces_shift)
    for iy = 1:length(pieces_shift)
    for iz = 1:length(pieces_shift)
        type_x(ix,iy,iz) = pieces_shift(ix);
        type_y(ix,iy,iz) = pieces_shift(iy);
        type_z(ix,iy,iz) = pieces_shift(iz);
    end
    end
    end
    kernels = length(coeff);

    %Set up DG basis
    data.M = order;
    data.space_dims = 3;
    %data.r_param = 0;
    data.Neqns = 1;
    data.basis = 'canonical' ;
    data.predictorbasis = 'Q';
    data.correctorbasis = 'P';
    [data] = initialize_Basis(data,data.space_dims);    
    
    for kay = 1:data.theta
        for eqn = 1:data.Neqns
        ells = data.sysBOs(eqn,kay);
        coeffs = data.sysBOscoeffs(eqn,kay);   
        quadpoint = [xii,eta,zeta];
        %Phi terms
        vectphi(eqn,kay) =  testfunction_phi(quadpoint,ells,data).*coeffs;
        end
    end
    
 maxdeltacell = pieces/2 + (length(coeff)-1)/2;
    DGcells = floor([-maxdeltacell -(maxdeltacell-1):.5:(maxdeltacell-.5) maxdeltacell]);
    for ix = 1:length(DGcells)
    for iy = 1:length(DGcells)
    for iz = 1:length(DGcells)
        DGxcells(ix,iy,iz) = DGcells(ix);
        DGycells(ix,iy,iz) = DGcells(iy);
        DGzcells(ix,iy,iz) = DGcells(iz);
    end
    end
    end
    
    for iqx = 1:length(quadlocs) 
    for iqy = 1:length(quadlocs) 
    for iqz = 1:length(quadlocs) 
        
    Qx = quadlocs(iqx);
    Qy = quadlocs(iqy);
    Qz = quadlocs(iqz);
        
    Cx = sym('Cx',[1,order]);
    Cy = sym('Cy',[1,order]);
    Cz = sym('Cz',[1,order]);

    
%%{
    %Set up arbitrary filter coefficients
    px = Cx*xtilde.^[(order-1):-1:0]';
    py = Cy*ytilde.^[(order-1):-1:0]';
    pz = Cz*ztilde.^[(order-1):-1:0]';

    xtilde1 = (1-Qx)/2*qx;
    xtilde2 = (1+Qx)/2*qx+(1-Qx)/2;
    ytilde1 = (1-Qy)/2*qy;
    ytilde2 = (1+Qy)/2*qy+(1-Qy)/2;
    ztilde1 = (1-Qz)/2*qz;
    ztilde2 = (1+Qz)/2*qz+(1-Qz)/2;

    clear filter    
    %Bounds Type 1 in x Type 1 in y
    integrandx1  = simplify(int(subs(px*vectphi,[xii xtilde],[(1-Qx)*qx+Qx xtilde1]),qx,0,1));
    integrandx2  = simplify(int(subs(px*vectphi,[xii xtilde],[(1-Qx)*qx+Qx xtilde2]),qx,0,1));
    
    disp('done x')

    integrandx1y1  = simplify(int(collect(subs(py*integrandx1,[eta ytilde],[(1-Qy)*qy+Qy ytilde1]),qy),qy,0,1));
    integrandx2y1  = simplify(int(collect(subs(py*integrandx2,[eta ytilde],[(1-Qy)*qy+Qy ytilde1]),qy),qy,0,1));

    integrandx1y2  = simplify(int(collect(subs(py*integrandx1,[eta ytilde],[(1+Qy)*qy-1 ytilde2]),qy),qy,0,1));
    integrandx2y2  = simplify(int(collect(subs(py*integrandx2,[eta ytilde],[(1+Qy)*qy-1 ytilde2]),qy),qy,0,1));

    disp('done y')
    integrandx1y1z1  = (1-Qz)/2*int(collect(subs(pz*integrandx1y1,[zeta ztilde],[(1-Qz)*qz+Qz ztilde1]),qz),qz,0,1);
    integrandx2y1z1  = (1-Qz)/2*int(collect(subs(pz*integrandx2y1,[zeta ztilde],[(1-Qz)*qz+Qz ztilde1]),qz),qz,0,1);
    integrandx1y2z1  = (1-Qz)/2*int(collect(subs(pz*integrandx1y2,[zeta ztilde],[(1-Qz)*qz+Qz ztilde1]),qz),qz,0,1);
    integrandx2y2z1  = (1-Qz)/2*int(collect(subs(pz*integrandx2y2,[zeta ztilde],[(1-Qz)*qz+Qz ztilde1]),qz),qz,0,1);

    disp('done half of z')
    integrandx1y1z2  = int(collect(subs(pz*integrandx1y1,[zeta ztilde],[(1+Qz)*qz-1 ztilde2]),qz),qz,0,1);
    integrandx2y1z2  = int(collect(subs(pz*integrandx2y1,[zeta ztilde],[(1+Qz)*qz-1 ztilde2]),qz),qz,0,1);
    integrandx1y2z2  = int(collect(subs(pz*integrandx1y2,[zeta ztilde],[(1+Qz)*qz-1 ztilde2]),qz),qz,0,1);
    integrandx2y2z2  = int(collect(subs(pz*integrandx2y2,[zeta ztilde],[(1+Qz)*qz-1 ztilde2]),qz),qz,0,1);
    disp('done z')
    
    filter(:,:,1,1,1) =  (1-Qx)/2*(1-Qy)/2*(1-Qz)/2*integrandx1y1z1; 
    filter(:,:,2,1,1) =  (1+Qx)/2*(1-Qy)/2*(1-Qz)/2*integrandx2y1z1;
    filter(:,:,1,2,1) =  (1-Qx)/2*(1+Qy)/2*(1-Qz)/2*integrandx1y2z1;
    filter(:,:,2,2,1) =  (1+Qx)/2*(1+Qy)/2*(1-Qz)/2*integrandx2y2z1;
    filter(:,:,1,1,2) =  (1-Qx)/2*(1-Qy)/2*(1+Qz)/2*integrandx1y1z2;
    filter(:,:,2,1,2) =  (1+Qx)/2*(1-Qy)/2*(1+Qz)/2*integrandx2y1z2;
    filter(:,:,1,2,2) =  (1-Qx)/2*(1+Qy)/2*(1+Qz)/2*integrandx1y2z2;
    filter(:,:,2,2,2) =  (1+Qx)/2*(1+Qy)/2*(1+Qz)/2*integrandx2y2z2;  

    clear bspline
    for ix = 1:(2*pieces)
    for iy = 1:(2*pieces)
    for iz = 1:(2*pieces)
        mat = filter(:,:,type_x(ix,iy,iz),type_y(ix,iy,iz),type_z(ix,iy,iz));
        matsubbed = double(subs(mat,[Cx Cy Cz], ...
            [bscoeffs(coeff_x(ix,iy,iz),:) bscoeffs(coeff_y(ix,iy,iz),:) bscoeffs(coeff_z(ix,iy,iz),:)]));
            bspline(:,:,ix,iy,iz) = matsubbed;
    end
    end
    donesubbbing = ix/(2*pieces)
    end

    fullfilter = zeros(data.Neqns,data.theta,2*(kernels-1)+2*pieces,2*(kernels-1)+2*pieces,2*(kernels-1)+2*pieces);
    
    %%{
    for cx = 1:kernels
    for cy = 1:kernels
    for cz = 1:kernels
        icx = 2*(cx-1);
        icy = 2*(cy-1);
        icz = 2*(cz-1);
        for ix = 1:(2*pieces)
        for iy = 1:(2*pieces)
        for iz = 1:(2*pieces)
        fullfilter(:,:,icx+ix,icy+iy,icz+iz) = fullfilter(:,:,icx+ix,icy+iy,icz+iz) + bspline(:,:,ix,iy,iz)*coeff(cx)*coeff(cy)*coeff(cz);
        end
        end
        end
    end
    end
    donepart1 = cx/kernels
    end
        quadlocsfilter(:,:,:,:,:,iqx,iqy,iqz) = double(fullfilter);
    end
    enn = length(quadlocs);
    donepart2 = (iqy+(iqx-1)*enn)/enn^2;
    waitbar(donepart2,h1,'Creating Filter')

    end    
    end
    %}
    filtername = ['filterdata/filterdata_' num2str(data.space_dims) 'D_M' num2str(data.M) '_' data.correctorbasis 'basis'];
    save(filtername,'quadlocsfilter','DGxcells','DGycells','DGzcells','quadlocs','quadwgts')

