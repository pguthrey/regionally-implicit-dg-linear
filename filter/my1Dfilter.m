function sum = my1Dfilter(DGsoln,cellx,data)
% Filters the DG solution coefficients using the DG coefficients
% of neighboring cells

Dgcellsx = mod(cellx + data.DGxcells - 1,data.Nv1) + 1;

ndg = length(Dgcellsx);
          
sum = 0;
for ix = 1:ndg
    cellcoeffs = DGsoln(:,Dgcellsx(ix));
    sum = sum + data.thisfilter(:,:,ix)*cellcoeffs;
end