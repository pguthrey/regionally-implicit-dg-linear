function sum = my3Dfilter(DGsoln,cellx,celly,cellz,data)
% Filters the DG solution coefficients using the DG coefficients
% of neighboring cells

Dgcellsx = mod(cellx + data.DGxcells - 1,data.Nv1) + 1;
Dgcellsy = mod(celly + data.DGycells - 1,data.Nv2) + 1;
Dgcellsz = mod(cellz + data.DGzcells - 1,data.Nv3) + 1;

[ndg,~] = size(Dgcellsx);
          
sum = 0;
for ix = 1:ndg
for iy = 1:ndg
for iz = 1:ndg
    cellcoeffs = DGsoln(:,Dgcellsx(ix,iy,iz),Dgcellsy(ix,iy,iz),Dgcellsz(ix,iy,iz));
    sum = sum + data.thisfilter(:,:,ix,iy,iz)*cellcoeffs;
end
end
end