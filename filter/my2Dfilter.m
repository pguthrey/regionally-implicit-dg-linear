function sum = my2Dfilter(DGsoln,cellx,celly,data)
% Filters the DG solution coefficients using the DG coefficients
% of neighboring cells

Dgcellsx = mod(cellx + data.DGxcells - 1,data.Nv1) + 1;
Dgcellsy = mod(celly + data.DGycells - 1,data.Nv2) + 1;

[ndg,~] = size(Dgcellsx);
          
sum = 0;
for ix = 1:ndg
for iy = 1:ndg
    cellcoeffs = DGsoln(:,Dgcellsx(ix,iy),Dgcellsy(ix,iy));
    sum = sum + data.thisfilter(:,:,ix,iy)*cellcoeffs;
end
end
