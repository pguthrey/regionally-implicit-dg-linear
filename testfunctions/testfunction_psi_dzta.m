function [ psi ] = testfunction_psi_dzta(quadpoint,ell,data)
% Converts sets of coefficients for the region to the system of equations
% written by data.Pierson Guthrey
% -------------------------------------------------
% INdata.PUTS    x          : the test coefficients of the lsqnonlin data.method
%           pastregion : the coefficients from the previous time 
% OUTdata.PUTS   sys        : the LHS of the system of eqns, ideally  sys=0
% Note: other variables may be %loaded in from the problem parameter files
% ------------------------------------------------------------------------
psi_zta = 1;
psi_eta = 1;

if ell ~=0
    ell_tau = data.BO(ell,1);
    tau = quadpoint(1);
    psi_tau = testfunction_Legendre(tau,ell_tau);
    ell_xii = data.BO(ell,2);
    xii = quadpoint(2);
    psi_xii = testfunction_Legendre(xii,ell_xii);
    if data.space_dims >= 2
        eta = quadpoint(3);
        ell_eta = data.BO(ell,3);
        psi_eta = testfunction_Legendre(eta,ell_eta);
        if data.space_dims >= 3
            zta = quadpoint(4);
            ell_zta = data.BO(ell,4);
            psi_zta = testfunction_LegendreDiff(zta,ell_zta);
        end
    end           
    psi = psi_tau.*psi_xii.*psi_eta.*psi_zta;
else
    psi = 0;
end
end