Regionally Implict Discontinuous Galerkin Method 
Example code for solving linear problems

To check the installation, run DG_method_main.m
The result should be some output about the method runtime and the relative error. 

Once you have verified that the installation was successful, navigate to to the problems folder. 
The goal is to copy a current problem directory and then modify it to contain the information for the problem you wish to solve. 
Begin by copying the problem directory $ccadvection1D$. Each file contains comments to explain how to adapt the program to your desired experiment. 
Here we provide an overview of how to fit the problem into the program.  Consider a conservation law
```
#!latex
q_t + f(q)_x + g(q)_y + h(q)_z = 0.
```
We assume that our conservation law is linear, so we have the problem
```
#!latex
q_t + f'(x,y,z)q_x + g'(x,y,z)q_y + h'(x,y,z)q_z = 0 .
```
Now, open the following files in the problem directory and input the needed information

problem_F - this is the problem's flux function in the $x$ direction, $f(q)$
problem_F_FluxJacobian - this is the problem's flux Jacobian in the x direction, $f'(q)= f'(x,y,z)$
problem_get_parameters - contains the problem domain and final time, as well as other pertiment experiment data.
problem_IC - contains a function to act as the initial conditions of the experiment
problem_solution - contains a function that is the exact solution to the problem (if applicable)

Note that there are files analogous to problem_F,..., for the flux functions in the $y$ and $z$ directions. 




