function [DGfinalsoln,data] = RIrDG_method_linear(DGsolution_old,data)
%Run the DG method for the given inputs

deltatx = data.deltav1/data.Fspeedmax*data.cfl;
deltaty = data.deltav2/data.Gspeedmax*data.cfl;
deltatz = data.deltav3/data.Hspeedmax*data.cfl;
deltat = min([deltatx deltaty deltatz]);
Nsteps = max([10 ceil(data.Tfinal/deltat)]);%take at least 10 time steps
deltat = data.Tfinal/Nsteps;
data.nuv1 = deltat/data.deltav1;
data.nuv2 = deltat/data.deltav2;
data.nuv3 = deltat/data.deltav3;

[update,update_index,Kmax] = initialize_linear_update(data);

if data.check_conservation
    [data] = check_conservation(DGsolution_old,data);
end

if data.makegif_conserved
    [data] = plotting_makegif(DGsolution_old,0,data,0);
elseif data.plotIC
    problem_plotting_IC(DGsolution_old,data) 
end

%h1 = %waitbar(0,'Running Experiment...','OuterPosition', [100 400 300 75]);
tnow = 0;
nstep = 2;
%We timestep until we have reached the final time or we cannot timestep
experiment_dt = [];
experiment_nstep = [];
experiment_time = [];
data.Time = 0;
tic;
DGsolution_new = NaN(data.theta,data.Nv1,data.Nv2,data.Nv3);
for nstep = 1:Nsteps
    for iv1 = 1:data.Nv1
    for iv2 = 1:data.Nv2
    for iv3 = 1:data.Nv3       
        cell = DGsolution_old(:,iv1,iv2,iv3);
        for update_k = 1:Kmax
            if ~isnan(update_index(1,update_k,iv1,iv2,iv3))
                iv1_tilde = update_index(1,update_k,iv1,iv2,iv3);
                iv2_tilde = update_index(2,update_k,iv1,iv2,iv3);
                iv3_tilde = update_index(3,update_k,iv1,iv2,iv3);
                U = update(:,:,update_k,iv1,iv2,iv3);
                cell = cell - U*DGsolution_old(:,iv1_tilde,iv2_tilde,iv3_tilde);
            end
        end
        DGsolution_new(:,iv1,iv2,iv3) = cell;            
    end
    end
    end    
    experiment_dt = [experiment_dt,deltat];
    experiment_nstep = [experiment_nstep,nstep-1];
    experiment_time = [experiment_time,tnow];
    if data.check_conservation
        [data] = check_conservation(DGsolution_new,data);
    end
    tnow = tnow + deltat;
    data.Time(nstep) = tnow ; 
    if data.makegif_conserved
        [data] = plotting_makegif(DGsolution_new,tnow,data,nstep);
    elseif data.plotwhilerunning 
        problem_plotting_whilerunning(DGsolution_new,tnow,data)
    end
    DGsolution_old = DGsolution_new;
    %done = tnow/data.Tfinal;
    %waitbar(done, h1);
end
data.runtime = toc;
if data.verbose
    disp('Experiment complete')
end
%delete(h1)

data.Nt = nstep-1;

DGfinalsoln = DGsolution_new;

if data.makegif_conserved
    [data] = plotting_makegif(DGsolution_new,tnow,data,nstep);
elseif data.plotfinal  
    problem_plotting_whilerunning(DGsolution_new,tnow,data)
end

filename = ['soln_' data.problemname '_r'  data.r_param '_M' num2str(data.M) '_N1' num2str(data.Nv1) '_N2' num2str(data.Nv2) '_N3' num2str(data.Nv3) '_' data.basis];
if exist(['results\' filename ],'dir')
    rmdir(['results\' filename ],'s');
end

end


