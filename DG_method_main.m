clc
clear all
close all force
dbstop if error
warning('off','all')

%See /problems to see list of problems
problemname = 'ccadvection3D';
%Method parameters
for M = [6] %Method order
for r = [0] %Region size, 0 for LIDG, 1 for RIDG
for N = [80] %cells in each dimension 

addpath(['problems/' problemname])
addpath('predictor')
addpath('corrector')
addpath('compute')
addpath('testfunctions')
addpath('initialize')
addpath('plotting')
addpath('filter')
addpath('filter/filterdata')
addpath('smartsolver')

%Initialize Solver data
clear data 
data.problemname = problemname;
[data] = problem_get_parameters(data);
data.Nv1 = N;
data.Nv2 = 1;
data.Nv3 = 1; 
if data.space_dims >= 2
    data.Nv2 = N;
    if data.space_dims>= 3
        data.Nv3 = N;
    end
end
data.basiscombine = 'full';
data.basis = 'canonical';
data.extra = 'nonlinconvergence1d';
data.M = M;
data.predictor_solver = 'NewtonIteration';
data.predictorbasis = 'Q';
data.correctorbasis = 'P';
data.r_param = r; 

%Options
data.verbose = true;
data.plotIC = false; 
data.plotwhilerunning = false;
data.plotfinal = false;
data.makegif_conserved = false;
data.filter = false;
data.smartsolver = true;
data.usewaitbars = true;
data.check_conservation = false;

close all force
[data] = initialize_method(data);
[data] = initialize_Basis(data,data.space_dims);
[data] = initialize_gauss_quadrature(data,data.M);
[data] = initialize_innerproduct_quadrature(data);
[data] = initialize_mesh(data);
[data] = initialize_precompute_testfunctions(data);
[data] = initialize_formIntegrals(data,data.Tfinal);        

%Project initial conditions onto DG basis
DG_initialconditions = projection_DGL2_Proj(@(point) problem_IC(point,data.appdata),data);
%Run method
M = M
r = r
N = N
tic
switch data.linearcase
    case 'constantcoefficient'
    [DGfinalsoln,data] = RIrDG_method_cc(DG_initialconditions,data);        
    case 'linear'
    [DGfinalsoln,data] = RIrDG_method_linear(DG_initialconditions,data);
    case 'nonlinear'
    %to be released at a later date
end
time = toc;
disp(['Runtime = ' num2str(time)])
if data.measureerror 
    if data.filter
        [ data ] = finalize_error_filtered(DGfinalsoln,data);
        disp(['L1 relative error = ' num2str(data.Rel_L1err)])
        disp(['L2 relative error = ' num2str(data.Rel_L2err)])
        disp(['Linf relative error = ' num2str(data.Rel_Lierr)])
        disp(['L1 relative error (filtered) = ' num2str(data.Rel_L1err_filtered)])
        disp(['L2 relative error (filtered) = ' num2str(data.Rel_L2err_filtered)])
        disp(['Linf relative error (filtered) = ' num2str(data.Rel_Lierr_filtered)])
    else
        [ data ] = finalize_error(DGfinalsoln,data);
        disp(['L1 relative error = ' num2str(data.Rel_L1err)])
        disp(['L2 relative error = ' num2str(data.Rel_L2err)])
        disp(['Linf relative error = ' num2str(data.Rel_Lierr)])
    end
else
    data.Rel_L1err = NaN;
    data.Rel_L2err = NaN;
    data.Rel_Lierr = NaN;
    data.Rel_L1err_filtered = NaN;
    data.Rel_L2err_filtered = NaN;
    data.Rel_Lierr_filtered = NaN;
end
disp('---------------------------------------------------')

end
end
end





