function [ gq ] = problem_G(q,quadpoint,appdata)
% Evaluates g(q) for the given problem
% written by Pierson Guthrey
% -------------------------------------------------
% INPUTS    q
% OUTPUTS   g evaluated for conserved variables g
% Note: other variables may be %loaded in from the problem files
% ------------------------------------------------------------------------

%NON-CC Advection
gq = quadpoint(1)*q;

end




