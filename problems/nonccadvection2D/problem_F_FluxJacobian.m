function [ Jacobian ] = problem_F_FluxJacobian(q,quadpoint,appdata)
% Evaluates the flux Jacobian for the given problem
% written by Pierson Guthrey
% -------------------------------------------------
% INPUTS    q
% OUTPUTS   Jacobian
% Note: other variables may be %loaded in from the problem files
% ------------------------------------------------------------------------

%NonCC Advection
Jacobian = -quadpoint(2);


end

