function [ exact ] = problem_solution(t,quadpoint,data)
% Computes the exact solution
% written by data.Pierson Guthrey
% -------------------------------------------------
% INPUTS    (t,v1,v2) : location in space-time 
% OUTPUTS   exact   : the given exact solution evaluated at the location
% Note: other variables may be %loaded in from the problem files
% ------------------------------------------------------------------------

% Non-CC Advection
x = quadpoint(1);
y = quadpoint(2);
xprime = cos(t).*x-sin(t).*y;
yprime = sin(t).*x+cos(t).*y; 

exact = problem_IC([xprime yprime],data.appdata);

end