function [ fq ] = problem_F(q,quadpoint,appdata)
% Evaluates f(q) for the given problem
% written by Pierson Guthrey
% -------------------------------------------------
% INPUTS    q
% OUTPUTS   f evaluated for conserved variables f
% Note: other variables may be %loaded in from the problem files
% ------------------------------------------------------------------------

%Non-CC advection
fq= -quadpoint(2)*q;
end



