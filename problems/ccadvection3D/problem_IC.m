function [ qIC ] = problem_IC(point,appdata)
% Evaluates the Initial Conditions for the given problem
% written by Pierson Guthrey
% -------------------------------------------------
% INPUTS    x,y     : locations at which the ICs are to be evaluated
% OUTPUTS   qIC     : ICs evaluated at the given locations
% Note: other variables may be %loaded in from the problem parameter files
% ------------------------------------------------------------------------re

x = point(1);
y = point(2);
z = point(3);

omega = 2;
qIC = sin(omega*pi*x).*sin(omega*pi*y).*sin(omega*pi*z);

end

