function [ soln ] = smartsolver(LHS,RHS,targetDoF)

newlist = targetDoF;
oldlist = [];
while length(newlist) > length(oldlist)
    oldlist = newlist;
    A = LHS(newlist,:);
    new = find(max(abs(A))> eps/100 );
    newlist = unique([oldlist new]);
end


LHStilde = LHS(newlist,newlist);
RHStilde = RHS(newlist,:);

solntilde = LHStilde\RHStilde;

soln = zeros(length(targetDoF),length(RHStilde(1,:)));
for ik = 1:length(targetDoF)
    soln(ik,:) = solntilde(newlist == targetDoF(ik),:);
end


end

